#!/usr/bin/perl -w

($P, $U, $G, $packets, $nops, $MaxNoComps)=@ARGV;

if(@ARGV!=6) {
    print STDERR "\n::: USAGE :::\n\n1) <continuous.phrase.0 (DIR)>\n2) <components.cont.0>\n3) <components.disc.0>\n4) <# packets>\n5) <# procs>\n6) <Maximum # components>\n\n";
    exit(-1);
}
use POSIX;
###########################################################################################################################################################

$packets = 2;

@handler=();
$packet_size = floor( $nops / $packets );
for ($k=0; $k<$packets; $k++ ) {
        $handler[$k][0] = $k*$packet_size+1;
        $handler[$k][1] = ($k+1)*$packet_size;
}
$handler[0][0] = 0;
$handler[$packets-1][1] = $nops;

###########################################################################################################################################################

for ($k=0; $k<$packets; $k++) {
        for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
		$pairs  = "$P/$P.$job";
                system("nohup ./compute-smooth.pl  $job  $pairs  $U  $G  $MaxNoComps  1>log.$job  2>>err.$job  &");
        }
	flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
}

system("mkdir  smoothWeights");
system("mv     smoothWeights.*   smoothWeights");

###########################################################################################################################################################

system("touch  DONE.SMOOTH");

###########################################################################################################################################################

sub flagging {

my      ($start, $finish, $name)=@_;
my      $job;
my      @flag_job=();
my      $flags=0;
my      $NoJ;

        $NoJ = $finish - $start + 1;

        for ($job=$start; $job <= $finish; $job++) { $flag_job[$job]=0; }

        while($flags != $NoJ) {
                for ($job=$start; $job <= $finish; $job++) {
                        if (-e "$name.$job") { $flag_job[$job]=1;}
                }
                $flags=0;
                for ($job=$start; $job <= $finish; $job++) {
                        $flags += $flag_job[$job];
                }
        }
        for ($job=$start; $job <= $finish; $job++) { qx(rm $name.$job);  }
        return();
}


