#!/usr/bin/perl -w

($C, $packets, $NoM, $nops, $type)=@ARGV;

if(@ARGV!=5) {
    print STDERR "\nUSAGE ::: <counts (DIR)>  <# packets>  <# measures>  <# procs>  <cont/disc>\n";
    exit(-1);
}
use POSIX;
###########################################################################################################################################################

@label = ('src', 'trg');

###########################################################################################################################################################

@handler=();
$packet_size = ceil( $nops / 2 );
for ($k=0; $k<2; $k++ ) {
        $handler[$k][0] = $k*$packet_size+1;
        $handler[$k][1] = ($k+1)*$packet_size;
}
$handler[0][0] = 0;
$handler[1][1] = $nops;

for ($k=0; $k<2; $k++) {
	for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
		system("nohup ./compute-local_Pair_Count.pl  $job  $C  2>>err.$job  &");
	}
	flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
}

for ($i=0; $i<=1; $i++) {
	$dir = $label[$i].'PhraseCount';
	system("mkdir  $dir");
	for ($k=0; $k<2; $k++) {
		for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
	        	system("nohup ./compute-local_Phrase_Count.pl  $job  $C  $dir  $label[$i]  2>>err.$job  &");
        	}
		flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
	}
}

###########################################################################################################################################################

@handler=();
$packet_size = ceil( $nops / $packets );
for ($k=0; $k<$packets; $k++ ) {
        $handler[$k][0] = $k*$packet_size+1;
        $handler[$k][1] = ($k+1)*$packet_size;
}
$handler[0][0] = 0;
$handler[$packets-1][1] = $nops;

for ($i=0; $i<=1; $i++) {
	$dir = $label[$i].'PhraseCount';
	for ($k=0; $k<$packets; $k++) {
        	for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
                	system("nohup ./compute-Phrase_Count.pl  $job  $dir  $label[$i]  $nops  2>>err.$job  &");
        	}
        	flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
	}
	for ($job=0; $job<=$nops; $job++) {	
		$file = "$dir/$dir.$job";	
		system("rm  $file");
		system("mv  temporaryPhrase.$label[$i].$job  $file");
	}
}

###########################################################################################################################################################

for ($k=0; $k<$packets; $k++) {
	for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
		system("nohup ./compute-insert_Phrase_counts.pl  $job  $C  2>>err.$job  &");
	}
	flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
}

###########################################################################################################################################################

#$pair_packets = $packets;
#for ($k=0; $k<$pair_packets; $k++ ) {
#        $handler[$k][0] = $k*$packet_size+1;
#        $handler[$k][1] = ($k+1)*$packet_size;
#}
#$handler[0][0] = 0;
#$handler[$pair_packets-1][1] = $nops;
#@handler=();
#@{ $handler[0] }  = (0,10);
#@{ $handler[1] }  = (11,21);
#@{ $handler[2] }  = (22,32);
#@{ $handler[3] }  = (33,43);
#@{ $handler[4] }  = (44,54);
#$pair_packets = 5;
@handler=();
@{ $handler[0] }  = (0,18);
@{ $handler[1] }  = (19,36);
@{ $handler[2] }  = (37,54);
$pair_packets = 3;

for ($k=0; $k<$pair_packets; $k++) {
	for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
		system("nohup ./compute-translation_probabilities.pl  $job  $C  $NoM  $nops  2>>err.$job  &");
	}
	flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
}

###########################################################################################################################################################

system("rm   -r  *PhraseCount");

for ($job=0; $job<=$nops; $job++) {
        $file = "$C/$C.$job";
        system("mv  translationProbabilities.$job  $file");
}

###########################################################################################################################################################

system("touch  DONE.EMPTP.$type");

###########################################################################################################################################################

sub flagging {

my      ($start, $finish, $name)=@_;
my      $job;
my      @flag_job=();
my      $flags=0;
my      $NoJ;

        $NoJ = $finish - $start + 1;

        for ($job=$start; $job <= $finish; $job++) { $flag_job[$job]=0; }

        while($flags != $NoJ) {
                for ($job=$start; $job <= $finish; $job++) {
                        if (-e "$name.$job") { $flag_job[$job]=1;}
                }
                $flags=0;
                for ($job=$start; $job <= $finish; $job++) {
                        $flags += $flag_job[$job];
                }
        }
        for ($job=$start; $job <= $finish; $job++) { qx(rm $name.$job);  }
        return();
}


