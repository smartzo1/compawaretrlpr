#!/usr/bin/perl -w 

use strict;

my ($proc, $pairsFile, $contCompsFile, $gappyCompsFile, $MaxNoComps) = @ARGV;

if(@ARGV!=5) {
   print STDERR "\n::: USAGE :::\n\n1) <proc #>\n2) <continuous.phrase.batch.proc>\n3) <components.cont.batch.proc>\n4) <components.disc.batch.proc>\n5) <Maximum # components>\n\n";
   exit(-1);
}


#############################################################################################################################################################

my %_UC_ = ();
my %_GC_ = (); 

getPairCounts(\%_UC_, $contCompsFile);
getPairCounts(\%_GC_, $gappyCompsFile);

my $log_counter = 1;
work(\%_UC_, \%_GC_, $proc, $log_counter, $pairsFile);

#############################################################################################################################################################
	
system("sort  -T .  smoothWeights.$proc  -o  smoothWeights.$proc");

system("touch  _WORK_.$proc");
	
#########################################################################################################################

sub getPairCounts {

my      ($_D_, $countFile) = @_;

my      $u;
my      @P;
my      @c;

        open(FIN, "<$countFile");
        while(<FIN>) {
                chomp;
                @P = ();
                @P = split(/ \|\|\| /);
                $u = "$P[0] ||| $P[1]";
                @c = ();
                @c = split(/ /, $P[2]);
                @{ $$_D_{$u} } = @c;
        }
        close(FIN);
}

#########################################################################################################################

sub work {

my	($_UC_, $_GC_, $proc, $log_counter, $pairsFile) = @_;

my	($line, $n, $pair, $p_G, $i);
my	@data;
my	@P;
my	@O;
my	@struct;
my	@L;
my	@pos2word;
my	@Probs;
my	%Output = ();

        open(TBP, "<$pairsFile");
        while($line=<TBP>) {
		
		chomp($line);
                @data = (); 
		@data = split(/ \|\|\| /, $line);
                @P = (); 
		@P = @data[0..1];
		$pair = ();
		$pair = "$P[0] ||| $P[1]";		
		if (!defined($Output{$pair})) {
			@{ $Output{$pair} } = (0,0);
		}

		$log_counter++;
		print "$log_counter\n";

		@pos2word = ();
		wordDataStruct(\@P, \@pos2word);

                $n = scalar(@data);

		@O = (); 
		@O = split(/ /, $data[$n-1]);
		$p_G = ();
		$p_G = $O[0]/$O[1];	

                @struct = (); 
		@struct = @data[2..$n-2];

                $n = scalar(@struct);

                @L = (); 
		@L = @struct[$n/2..$n-1];
		expandGAPs(\@L);
	
		@Probs = ();
		@Probs = (1,1);
	
		computeProbabilities(\@L, \@pos2word, $_UC_, $_GC_, \@Probs);

		for ($i=0; $i<=1; $i++) {
			$Output{$pair}[$i] += $p_G * $Probs[$i];
		}

		if ($log_counter == 100000) {
			if ($O[0] == $O[1]) {
				printOutput(\%Output, $proc);
				%Output = ();
				$log_counter++;
			}
		}
		else {
			$log_counter++;
		}

	}
	close(TBP);
	
	if (scalar(keys(%Output)) > 0) { printOutput(\%Output, $proc); }

}

#########################################################################################################################

sub computeProbabilities {

my	($L, $pos2word, $_UC_, $_GC_, $Probs) = @_;

my	($k, $i, $j, $pair);

my	@P;
my	@labels;
my	@tokens;
my	@list;

        for ($k=0; $k<scalar(@$L); $k++) {

		@labels = ();
		@labels = split(/ XXX /, $$L[$k]);
		@P = ();
		for ($i=0; $i<=1; $i++) {
			@tokens = ();
			@tokens = split(/ /, $labels[$i]);
			@list = ();
			for ($j=0; $j<scalar(@tokens); $j++) {
				push( @list, $$pos2word[$i]{$tokens[$j]} );
			}
			$P[$i] = join(' ', @list);
		}
		$pair = "$P[0] ||| $P[1]";
		
		if ($pair !~ /GAP/) {
			trlProbability($_UC_, \@P, $pair, $Probs);
		}
		else {
			trlProbability($_GC_, \@P, $pair, $Probs);
		}
	}
}

#########################################################################################################################

sub trlProbability {

my	($_D_, $P, $pair, $Probs) = @_;

my	$i;
my	$jointCount = ();
my	$sideCount = ();
my	$condProb = ();

if (!defined($$_D_{$pair}[0])) { print "$pair\n"; }

	$jointCount = $$_D_{$pair}[0];
        for ($i=0; $i<=1; $i++) {
        	if ($$P[$i] eq 'NULL') { next; }
                $sideCount = $$_D_{$pair}[$i+1];
                $condProb  = $jointCount / $sideCount;
                $$Probs[$i] *= $condProb;
        }
}

#########################################################################################################################

sub wordDataStruct {

my      ($P, $pos2word) = @_;

my      ($i, $j);
my      @tokens;

        for ($i=0; $i<=1; $i++) {
                @tokens = ();
                @tokens = split(/ /, $$P[$i]);
                for ($j=0; $j<scalar(@tokens); $j++) {
                        $$pos2word[$i]{$j} = $tokens[$j];
                }
                $$pos2word[$i]{'NULL'} = 'NULL';
		$$pos2word[$i]{'GAP'} = 'GAP';
        }
}

#########################################################################################################################

sub fewerDigits {

my      ($x, $NoD)=@_;
my      ($integer, $fractional, $less, $mantissa, $exponent);

        if ($x !~ 'e') {
                ($integer, $fractional) = split(/\./, $x);
                if (defined($fractional)) {
                        if (length($fractional) > $NoD) {
                                $less = substr($fractional, 0, $NoD);
                                $x = "$integer.$less";
                        }
                }
        }
        else {
                ($mantissa, $exponent) = split(/e-/, $x);
                ($integer, $fractional) = split(/\./, $mantissa);
                if (defined($fractional)) {
                        if (length($fractional) > $NoD) {
                                $less = substr($fractional, 0, $NoD);
                                $x = "$integer.$less".'e-'.$exponent;
                        }
                }
        }
        return($x);
}

#########################################################################################################################

sub printOutput {

my	($Output, $proc) = @_;

my	($pair, $i);
my	@prob;

        open(PRB, ">>smoothWeights.$proc");
        foreach $pair (keys(%$Output)) {
        	@prob = ();
                for ($i=0; $i<=1; $i++) {
			#$prob[$i] = $$Output{$pair}[$i];
                	$prob[$i] = fewerDigits($$Output{$pair}[$i], 8);
                }
                print PRB "$pair ||| $prob[1] $prob[0]\n";
        }
        close(PRB);
}

#########################################################################################################################

sub expandGAPs {

my	($labelStruct) = @_;

my	($c, $i, $j, $diff, $g, $l, $e_c);
my	@comp;
my	@l_c;
my	@labels;
my	@list;
my	@dummy;
my	@store = ();

	foreach $c (@$labelStruct) {
		$c =~ s/GAP //g;
		@comp = ();
		@comp = split(/ XXX /, $c);
		@l_c = ();
		for ($i=0; $i<=1; $i++) {
			@list   = ();
			@labels = ();
			@labels = split(/ /, $comp[$i]);
			push( @list, $labels[0] );
			for ($j=1; $j<scalar(@labels); $j++) {
				$l = $labels[$j];
				$diff = $labels[$j] - $labels[$j-1];
				if ($diff != 1) {
					@dummy = ();
					for ($g=1; $g<$diff; $g++) {
						push(@dummy , 'GAP');
					}
					push(@dummy, $l);
					$l = ();
					$l = join(' ', @dummy);
				}
				push( @list, $l);
			}
			$l_c[$i] = join(' ', @list); 
		}
		$e_c = ();
		$e_c = join(' XXX ', @l_c);
		push( @store, $e_c);
	}
	
	@$labelStruct = ();
	@$labelStruct = @store;
}

#########################################################################################################################

