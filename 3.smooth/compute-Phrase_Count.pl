#!/usr/bin/perl -w

($proc, $dir, $side, $nops) = @ARGV;

if(@ARGV!=4) {
    print STDERR "\nUSAGE ::: <proc #>   <'side'PhraseCount>   <side>  <# procs>\n";
    exit(-1);
}
########################################################################################################

$file = "$dir/$dir.$proc";
%H = ();
open(IN, "<$file");
while(<IN>) {
	chomp;
	@P = ();
	@P = split(/ \|\|\| /);
	@c = ();
	@c = split(/ /, $P[1]);
	@{ $H{$P[0]} } = @c ;
}
close(IN);

for ($i=0; $i<=$nops; $i++) {
	if ($i == $proc) { next; }
	$_file = "$dir/$dir.$i";
	open(IN, "<$_file");
	while(<IN>) {
		chomp;
		@P = ();
		@P = split(/ \|\|\| /);
		if (!defined($H{$P[0]})) { next; }
		@c = ();
		@c = split(/ /, $P[1]);
		for ($k=0; $k<@c; $k++) { $H{$P[0]}[$k] += $c[$k]; }
	}
	close(IN);
}

open(OUT, ">temporaryPhrase.$side.$proc");
foreach $u (keys(%H)) { print OUT "$u ||| @{ $H{$u} }\n"; }
close(OUT);

system("touch  _WORK_.$proc");
 


