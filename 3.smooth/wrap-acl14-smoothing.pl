#!/usr/bin/perl -w

($ID, $batch, $packets, $nops, $MaxNoC)=@ARGV;

if(@ARGV!=5) {
    print STDERR "\n::: USAGE :::\n\n1) <x.id.phrase.0 DIR)>\n\n2) <batch #>\n\n3) <# packets>\n\n4) <# procs>\n\n5) <Maximum # components>\n\n\n";
    exit(-1);
}
use POSIX;

############################################################################################################################################

$NoMeas = 1;

############################################################################################################################################
############################################################################################################################################

system("nohup ./wrap-components_only.pl  $ID  $batch  $packets  $nops  &");

while(!(-e 'DONE.COMPSONLY')) { sleep(1); }
system("rm  DONE.COMPSONLY");

############################################################################################################################################

$U = "components.cont.$batch"; 
$G = "components.disc.$batch";
@C = ();
@C = ($U, $G);
@log = ();
@log = ('cont', 'disc');

for ($i=0; $i<=1; $i++) {

	system("nohup ./wrap-translation_probabilities.pl  $C[$i]  $packets  $NoMeas  $nops  $log[$i]  &");

	while(!(-e "DONE.EMPTP.$log[$i]")) { sleep(1); }
	system("rm  DONE.EMPTP.$log[$i]");

}
############################################################################################################################################

system("nohup ./wrap-phrase2gap_update_side_counts.pl  $U  $G  $packets  $nops  &");

while(!(-e 'DONE.P2GUPD')) { sleep(1); }
system("rm  DONE.P2GUPD");

for ($i=0; $i<=1; $i++) {
	system("cat            components.$log[$i].$batch/* >> temp.comps.$log[$i].$batch");
	system("sort -T . -u   temp.comps.$log[$i].$batch   -o   temp.comps.$log[$i].$batch");
	system("rm   -r        components.$log[$i].$batch");
	system("mv             temp.comps.$log[$i].$batch   components.$log[$i].$batch"); 
}

############################################################################################################################################

system("nohup ./wrap-smooth.pl  continuous.phrase.0  $U  $G  $packets  $nops  $MaxNoC  &");

while(!(-e 'DONE.SMOOTH')) { sleep(1); }
system("rm  DONE.SMOOTH");

############################################################################################################################################

