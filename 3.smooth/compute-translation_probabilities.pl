#!/usr/bin/perl -w

($proc, $C, $NoM, $nops) = @ARGV;

if(@ARGV!=4) {
    print STDERR "\nUSAGE ::: <proc #>   <counts (DIR)>   <# measures>   <# procs>\n";
    exit(-1);
}
#############################################################################################################################

$M = $NoM-1;

$file = "$C/$C.$proc";
%H = ();
open(IN, "<$file");
while(<IN>) {
	chomp;
	@P = ();
	@P = split(/ \|\|\| /);
	$s = $P[0];  
	$t = $P[1];
	@counts = ();  
	@counts = split(/ /, $P[2]);
	@c_st = (); @n_s = (); @n_t = ();

        for ($k=0; $k<=$M; $k++)            { $c_st[$k]         = $counts[$k]; }

        for ($k=$M+1; $k<=2*$M+1; $k++)     { $n_s[$k-$M-1]     = $counts[$k]; }

        for ($k=2*($M+1); $k<=3*$M+2; $k++) { $n_t[$k-2*($M+1)] = $counts[$k]; }

	#for ($k=0; $k<=3; $k++)  { $c_st[$k]   = $counts[$k]; }
	#for ($k=4; $k<=7; $k++)  { $n_s[$k-4]  = $counts[$k]; }
	#for ($k=8; $k<=11; $k++) { $n_t[$k-8]  = $counts[$k]; }
 
	@{ $H{"$s ||| $t"}[0] } = @c_st;
	@{ $H{"$s ||| $t"}[1] } = @n_s;
	@{ $H{"$s ||| $t"}[2] } = @n_t;
}
close(IN);

for ($i=0; $i<=$nops; $i++) {
	if ($i == $proc) { next; }
	$_file = "$C/$C.$i";
	open(IN, "<$_file");
	while(<IN>) {
		chomp;
	        @P = ();
        	@P = split(/ \|\|\| /);
		$s = $P[0];  
		$t = $P[1];
		if (!defined($H{"$s ||| $t"})) { next; }
		@counts = ();  
		@counts = split(/ /, $P[2]);
		@c_st = ();
		for ($k=0; $k<=$M; $k++) { $c_st[$k] = $counts[$k]; }
		for ($k=0; $k<=$M; $k++) { $H{"$s ||| $t"}[0][$k] += $c_st[$k]; }
	}
	close(IN);
}

open(OUT, ">translationProbabilities.$proc");
foreach $pair (keys(%H)) {

	@n_st=(); @n_s=(); @n_t=();

        @n_st = @{ $H{$pair}[0] };
        @n_s  = @{ $H{$pair}[1] };
        @n_t  = @{ $H{$pair}[2] };

	#@p_st=(); @p_ts=();
	#for ($k=0; $k<=$M; $k++) { $p_st[$k] = fewerDigits($n_st[$k] / $n_t[$k], 8); }
	#for ($k=0; $k<=$M; $k++) { $p_ts[$k] = fewerDigits($n_st[$k] / $n_s[$k], 8); }

	print OUT "$pair ||| $n_st[0] $n_s[0] $n_t[0]\n";
        #print OUT "$pair ||| @p_st @p_ts\n";
}
close(OUT);

system("sort  translationProbabilities.$proc  -o  translationProbabilities.$proc");

system("touch  _WORK_.$proc");

########################################################################################################

sub fewerDigits {

my	($x, $NoD)=@_;
my	($integer, $fractional, $less, $mantissa, $exponent);

	if ($x !~ 'e') {
	        ($integer, $fractional) = split(/\./, $x);
        	if (defined($fractional)) {
                	if (length($fractional) > $NoD) {
                        	$less = substr($fractional, 0, $NoD);
	                        $x = "$integer.$less";
        	        }
        	}
	}
	else {
		($mantissa, $exponent) = split(/e-/, $x);
		($integer, $fractional) = split(/\./, $mantissa);
		if (defined($fractional)) {
			if (length($fractional) > $NoD) {
				$less = substr($fractional, 0, $NoD);
				$x = "$integer.$less".'e-'.$exponent;
			}
		}
	}
	return($x);
}

