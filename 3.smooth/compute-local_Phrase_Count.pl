#!/usr/bin/perl -w

($proc, $C, $dir, $side) = @ARGV;

if(@ARGV!=4) {
    print STDERR "\nUSAGE ::: <proc #>   <counts (DIR)>   <'side'PhraseCount>   <side>\n";
    exit(-1);
}
########################################################################################################

if    ($side eq 'src') {  
	$input  = "$C/$C.$proc";
	$pos = 0;
}
elsif ($side eq 'trg') {  
	$input = "$C/trg$C.$proc"; 
	$pos = 1;
}
$output = "$dir.$proc";

$prev_phrase = 'VOID';
@accum_count = ();
open(IN, "<$input");
open(OUT, ">$output");
while(<IN>) {
        chomp;
	@V = ();
        @V = split(/ \|\|\| /);

	@count = ();
	@count = split(/ /, $V[2]);

        if ($prev_phrase eq 'VOID') { 
		$prev_phrase = $V[$pos];
		for ($k=0; $k<scalar(@count); $k++) { $accum_count[$k] = $count[$k]; } 
		next; 
	}
        $phrase = $V[$pos];

        if ($prev_phrase eq $phrase) { 
		for ($k=0; $k<scalar(@count); $k++) { $accum_count[$k] += $count[$k]; }  
		next; 
	}

        print OUT "$prev_phrase ||| @accum_count\n";
        $prev_phrase = $phrase;
	@accum_count = ();
        @accum_count = @count;
}
close(IN);
print OUT "$prev_phrase ||| @accum_count\n";
close(OUT);

if ($side eq 'trg') { system("rm  $C/trg$C.$proc"); }

system("mv  $output  $dir");

system("touch  _WORK_.$proc");

