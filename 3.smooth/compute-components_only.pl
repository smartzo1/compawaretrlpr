#!/usr/bin/perl -w 

use strict;

my ($proc, $batch, $countsFile) = @ARGV;

if(@ARGV!=3) {
    print STDERR "\nUSAGE ::: <proc #>  <batch #>  <x.id.labeled.phrase.batch.proc>\n";
    exit(-1);
}

#############################################################################################################################################################

my $ID = 1;
my @f;
my %Z;

open(IN, "<$countsFile");
while(<IN>) {
        chomp;
        @f = ();
        @f = split(/ \|\|\| /);
        if ($f[0] != $ID) {
                work(\%Z, $ID);
                $ID = $f[0];
                print "{$ID}\n";
                %Z = ();
        }
        @{ $Z{"$f[1] ||| $f[2]"} } = @f[3..scalar(@f)-1];
}
close(IN);
work(\%Z, $ID);

#########################################################################################################################

system("touch _WORK_.$proc");
	
#########################################################################################################################

sub work {

my	($Z, $ID) = @_;

my	($input, $o, $out);
my	@P;
my	@struct;
my	@L;
my	@hash2pos;
my	@pos2word;
my	@word2hash;
my	%O = ();
my	@new_struct;

	foreach $input (keys(%$Z)) {

		@P = ();
		@P = split(/ \|\|\| /, $input);
		@struct = ();
		@struct = @{ $$Z{$input} };
                
		@new_struct = ();
                newStruct(\@struct, \@new_struct);
		eachUnalignedWordGetsNULL(\@new_struct);
		@{ $$Z{$input} } = ();
		@{ $$Z{$input} } = @new_struct;

		@hash2pos = ();
		@pos2word = ();
		@word2hash = ();
		@L = ();

		wordDataStruct(\@P, \@L, \@new_struct, \@hash2pos, \@pos2word, \@word2hash);
		allSubPairs(\@L, \@pos2word, \@word2hash, \%O);
	}

	#open(XID, ">>x.id.phrase.$batch.$proc");
	#foreach $input (keys(%$Z)) {
	#	$out = ();
	#	$out = join(' ||| ', @{ $$Z{$input} });
	#	print XID "$ID ||| $input ||| $out\n";
	#}
	#close(XID);

	open(CPP, ">>components.cont.$batch.$proc");
	open(DPP, ">>components.disc.$batch.$proc");
	foreach $o (keys(%O)) {
		if ($o =~ 'GAP') {
			$o =~ s/\#\d+//g;
			print DPP "$o ||| 1\n";
		}
		else {
			$o =~ s/\#\d+//g;
			print CPP "$o ||| 1\n";
		}
	}
	close(CPP);
	close(DPP);
}

#########################################################################################################################

sub allSubPairs {

my	($L, $pos2word, $word2hash, $O) = @_;

my	($n, $j, $full_string, $string, $k, $bit, $i, $l, $NoC, $flag);

my	@comp;
my	@loc_labels;
my	@labels;
my      @CheckBothNULL;

        $n = scalar(@$L);
        for ($j=1; $j<2**$n; $j++) {
                $full_string = dec2bin($j);
                $string = substr($full_string, -$n, $n );
		@labels = ();
		$NoC = 0;
                @CheckBothNULL = ();
                $flag = 1;
                for ($k=0; $k<$n; $k++) {
                        $bit = substr($string, $k, 1);
			$NoC += $bit;
                        if ($bit eq "0") { next; }
			push( @CheckBothNULL, $$L[$k] );
			@comp = ();
			@comp = split(/ XXX /, $$L[$k]);
			for ($i=0; $i<=1; $i++) {
				$comp[$i] =~ s/GAP //g;
				@loc_labels = ();
				@loc_labels = split(/ /, $comp[$i]);
				for ($l=0; $l<scalar(@loc_labels); $l++) {
					if ($loc_labels[$l] eq 'NULL') {
						$loc_labels[$l] = ();
						$loc_labels[$l] = 99;
					}
				}
				push( @{ $labels[$i] }, @loc_labels);
			}		
                }

		if    ($NoC > 1) { $flag = 0; }
                #elsif ($NoC == 2) { $flag = checkUnaligned(\@CheckBothNULL); }
                #if ($NoC > 1) { $flag = checkUnaligned(\@CheckBothNULL); }
                #if ($NoC == 2) { $flag = checkUnaligned(\@CheckBothNULL); }

	        if ($flag == 0) { next; }

		labels2pair(\@labels, $pos2word, $word2hash, $O);
        }
}

#########################################################################################################################

sub labels2pair {

my	($labels, $pos2word, $word2hash, $O) = @_;

my	($i, $j, $w, $v, $diff, $g);
my	@sorted_labels = ();
my	@list_pair = ();
my	@pair = ();
my	@dummy;

	for ($i=0; $i<=1; $i++) {

		@{ $sorted_labels[$i] } = sort { $a <=> $b } @{ $$labels[$i] };

		$w = $$pos2word[$i]{$sorted_labels[$i][0]};
		$v = $$word2hash[$i]{$w}{$sorted_labels[$i][0]};
		push( @{ $list_pair[$i] }, $v);	

		if ($w eq 'NULL') {
			next; 
		}

		for ($j=1; $j<scalar(@{ $sorted_labels[$i] }); $j++) {

			$w = $$pos2word[$i]{$sorted_labels[$i][$j]};

			if ($w eq 'NULL') { last; }

			$v = $$word2hash[$i]{$w}{$sorted_labels[$i][$j]};

			$diff = $sorted_labels[$i][$j] - $sorted_labels[$i][$j-1];

			if ($diff != 1) {
				@dummy = ();
				for ($g=1; $g<$diff; $g++) { 
					push(@dummy , 'GAP');
				}
				push(@dummy, $v);
				$v = ();
				$v = join(' ', @dummy);
			}

			push( @{ $list_pair[$i] }, $v);
		}
	}

	for ($i=0; $i<=1; $i++) { $pair[$i] = join(' ', @{ $list_pair[$i] } ); }

	$$O{"$pair[0] ||| $pair[1]"} = 'def';
}

#########################################################################################################################

sub dec2bin { return unpack("B32", pack("N", shift)); }

#########################################################################################################################

sub wordDataStruct {

my      ($P, $L, $struct, $hash2pos, $pos2word, $word2hash) = @_;

my      ($i, $j, $k, $w, $v, $c);
my      @tokens;
my	@cc;
my	@c;
my	@l_c;
my	@l_cc;

        for ($i=0; $i<=1; $i++) {
                @tokens = ();
                @tokens = split(/ /, $$P[$i]);
                for ($j=0; $j<scalar(@tokens); $j++) {
			$w = ();
			$w = $tokens[$j];
			$$hash2pos[$i]{$w} = $j;
			$v = ();
			$v = $w;
			$v =~ s/\#\d+//;
			$$word2hash[$i]{$v}{$j} = $w;
			$$pos2word[$i]{$j} = $v;	
                }
		$$hash2pos[$i]{'NULL'} = 'NULL';
		$$hash2pos[$i]{'GAP'} = 'GAP';
		$$word2hash[$i]{'NULL'}{99} = 'NULL';
                $$pos2word[$i]{99} = 'NULL';
        }

	for ($k=0; $k<scalar(@$struct); $k++) {
		@cc = ();
		@cc = split(/ XXX /, $$struct[$k]);
		@l_cc = ();
		for ($i=0; $i<=1; $i++) {
			@c = ();
			@c = split(/ /, $cc[$i]);
			@l_c = ();
			for ($j=0; $j<scalar(@c); $j++) {
				$w = ();
				$w = $c[$j];
				push( @l_c, $$hash2pos[$i]{$w} );
			}
			$l_cc[$i] = join(' ', @l_c);
		}
		$$L[$k] = "$l_cc[0] XXX $l_cc[1]";
	}
}

#########################################################################################################################

sub checkUnaligned {

my      ($CheckBothNULL) = @_;

my      ($i, $j, $k);
my      @comp;
my      $flag = 0;
my	@sideNull = (0,0);

        for ($k=0; $k<scalar(@$CheckBothNULL); $k++) {
                @comp = ();
                @comp = split(/ XXX /, $$CheckBothNULL[$k]);
                for ($i=0; $i<=1; $i++) {
                        if ($comp[$i] eq 'NULL') { 
				$flag++;
				$sideNull[$i] = 1;
			}
                }
        }

        if ($flag == scalar(@$CheckBothNULL)) { 
		if ($sideNull[0] + $sideNull[1] == 1) { return(1); }
		else				      { return(0); }
	}
        else { 
		return(1); 
	}
}

#sub checkUnaligned {
#
#my      ($CheckBothNULL) = @_;
#
#my      ($i, $j, $k);
#my      @comp;
#my      @Flag = ('FOO','FOO');
#
#        for ($k=0; $k<=1; $k++) {
#                @comp = ();
#                @comp = split(/ XXX /, $$CheckBothNULL[$k]);
#                for ($i=0; $i<=1; $i++) {
#                        if ($comp[$i] eq 'NULL') { $Flag[$i] = 'NULL'; }
#                }
#        }
#
#        if ( ($Flag[0] eq 'NULL') && ($Flag[1] eq 'NULL') ) { return(0); }
#        else                                                { return(1); }
#}
#
#########################################################################################################################

sub newStruct {

my      ($struct, $new_struct) = @_;

my      ($j, $null, $i, $u, $k, $temp) = @_;

my      @comp;
my      @tokens;
my      @store;
my      $gap = 0;

        for ($j=0; $j<scalar(@$struct); $j++) {
                if ($$struct[$j] =~ 'GAP') {
                        $gap = 1;
                }
                else {
                        push(@$new_struct, $$struct[$j]);
                        next;
                }
                @comp = ();
                @comp = split(/ XXX /, $$struct[$j]);
                $null = -1;
                for ($i=0; $i<=1; $i++) {
                        if ($comp[$i] eq 'NULL') { $null = $i; }
                }
                if ($null == -1) {
                        push(@$new_struct, $$struct[$j]);
                        next;
                }
                @tokens = ();
                @tokens = split(/ /, $comp[1-$null]);
                @store = ();
                $u = 0;
                push( @{ $store[$u] }, $tokens[0] );
                for ($k=1; $k<scalar(@tokens); $k++) {
                        if ($tokens[$k] eq 'GAP') {
                                $u++;
                                next;
                        }
                        push( @{ $store[$u] }, $tokens[$k] );
                }

                for ($u=0; $u<scalar(@store); $u++) {
                        @comp = ();
                        $comp[$null] = 'NULL';
                        $comp[1-$null] = join(' ', @{ $store[$u] } );
                        $temp = ();
                        $temp = "$comp[0] XXX $comp[1]";
                        push( @$new_struct, $temp);
                }
        }
}

#########################################################################################################################

sub eachUnalignedWordGetsNULL {

my      ($C) = @_;

my      ($k, $i, $null, $j);
my      @T = ();
my      @comp;
my      @labels;

        for ($k=0; $k<scalar(@$C); $k++) {
                @comp = ();
                @comp = split(/ XXX /, $$C[$k]);
                $null = -1;
                for ($i=0; $i<=1; $i++) {
                        if ($comp[$i] eq 'NULL') { $null = $i; }
                }
                if ($null == -1) {
                        push(@T, $$C[$k]);
                        next;
                }
                @labels = ();
                @labels = split(/ /, $comp[1-$null]);
                for ($j=0; $j<scalar(@labels); $j++) {
                        @comp = ();
                        $comp[1-$null] = $labels[$j];
                        $comp[$null] = 'NULL';
                        push(@T, "$comp[0] XXX $comp[1]");
                }
        }
        @$C = ();
        @$C = @T;
}

