#!/usr/bin/perl -w

($proc, $batch, $biCorp, $align, $lexS2T, $lexT2S)=@ARGV;

if(@ARGV!=6) {
    print STDERR "\nUSAGE ::: <proc #>  <batch #>  <biCorpus.batch.proc>  <alignments.batch.proc>  <lex.s2t.batch.proc>  <lex.t2s.batch.proc>\n";
    exit(-1);
}

#############################################################################################################################################

@C = ();
@lex = ($lexS2T, $lexT2S);
for ($i=0; $i<=1; $i++) {
	open(IN, "<$lex[$i]");
	while(<IN>) {
		chomp;
		($x, $y, $p) = split(/ \|\|\| /);
		if ($x eq '#') { $x = 'HASH'; }
		if ($y eq '#') { $y = 'HASH'; }
		$C[$i]{$x}{$y} = $p;
	}
	close(IN);
}	

$l_max = 1000;
$l = 0;
@SentPair = ();
@L = (); 
@line = ();
open(BIC,"<$biCorp");
open(ALG,"<$align");
open(OUT, ">batchCorpusLex.$batch.$proc");
while($line[0]=<BIC>, $line[1]=<ALG>) {
        chomp($line[0]);
        chomp($line[1]);

	$line[0] =~ s/\t//;
	$SentPair[$l] = $line[0];
	@tmp = ();
	@tmp = split(/ XXX /, $line[0]);

        @u = ();
        for ($i=0; $i<=1; $i++) {
                @{ $u[$i] } = split(/ /, $tmp[$i]);
        }

        @A = ();
        @A = split(/ /, $line[1]);
        @_A_ = ();
	foreach $a (@A) {
		($s_a, $t_a) = split(/-/, $a);
                push( @{ $_A_[0]{$s_a} }, $t_a );
                push( @{ $_A_[1]{$t_a} }, $s_a );
	}

	for ($i=0; $i<=1; $i++) {
		for ($j=0; $j<scalar(@{ $u[$i] }); $j++) {
                        $w = $u[$i][$j];
			$cw = $w;
			$cw =~ s/\#\d+//g;
                        if (!defined($_A_[$i]{$j})) {
				$L[$l][$i]{$w}{'NULL'} = $C[$i]{$cw}{'NULL'};
				$L[$l][1-$i]{'NULL'}{$w} = $C[1-$i]{'NULL'}{$cw};
                                next;
                        }
                        foreach $_j (@{ $_A_[$i]{$j} }) {
                                $_w = $u[1-$i][$_j];
				$_cw = $_w;
				$_cw =~ s/\#\d+//g;
				$L[$l][$i]{$w}{$_w} = $C[$i]{$cw}{$_cw};
                        }
		}
	}

	$l++;

	if ($l == $l_max) {
		for ($j=0; $j<$l_max; $j++) {
			print OUT "\t$SentPair[$j]\n";
			foreach $s (keys(%{$L[$j][0]})) {
				foreach $t (keys(%{$L[$j][0]{$s}})) {
					$p_t2s = $L[$j][1]{$t}{$s};
					$p_s2t = $L[$j][0]{$s}{$t};
					print OUT "$s ||| $t ||| $p_t2s $p_s2t\n";
				}
			}
		}
		@SentPair = ();
		@L = ();
		$l = 0;
	}
}
close(BIC); 
close(ALG);
if (scalar(@SentPair) > 0) {
	for ($j=0; $j<scalar(@SentPair); $j++) {
		print OUT "\t$SentPair[$j]\n";
		foreach $s (keys(%{$L[$j][0]})) {
			foreach $t (keys(%{$L[$j][0]{$s}})) {
				$p_t2s = $L[$j][1]{$t}{$s};
				$p_s2t = $L[$j][0]{$s}{$t};
				print OUT "$s ||| $t ||| $p_t2s $p_s2t\n";
			}
		}
	}
}
close(OUT);

system("touch _WORK_.$proc");
