#!/usr/bin/perl -w

($B, $batch, $packets, $nops)=@ARGV;

if(@ARGV!=4) {
    print STDERR "\nUSAGE ::: <biSegmentations.batch (DIR)>   <batch #>   <# packets>   <#procs>\n";
    exit(-1);
}
use POSIX;
############################################################################################################################################

$packet_size = floor( $nops / $packets );
@handler = ();
for ($k=0; $k<$packets; $k++ ) {
        $handler[$k][0] = $k*$packet_size+1;
        $handler[$k][1] = ($k+1)*$packet_size;
}
$handler[0][0]=0;
$handler[$packets-1][1] = $nops;

for ($k=0; $k<$packets; $k++) {
        for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
		$file  = "$B/*.$job";
		system("nohup ./compute-getBiCorpus.pl  $job  $batch  $file  &");
	}
	flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
}

system("mkdir  biCorpus.$batch");
system("mv     biCorpus.$batch.*  biCorpus.$batch");

#########################################################################################################################################

sub flagging {

my      ($start, $finish, $name)=@_;
my      $job;
my      @flag_job=();
my      $flags=0;
my      $NoJ;

        $NoJ = $finish - $start + 1;

        for ($job=$start; $job <= $finish; $job++) { $flag_job[$job]=0; }

        while($flags != $NoJ) {
                for ($job=$start; $job <= $finish; $job++) {
                        if (-e "$name.$job") { $flag_job[$job]=1;}
                }
                $flags=0;
                for ($job=$start; $job <= $finish; $job++) {
                        $flags += $flag_job[$job];
                }
        }
        for ($job=$start; $job <= $finish; $job++) { qx(rm $name.$job);  }
        return();
}
