#!/usr/bin/perl -w

($proc, $batch, $Bi)=@ARGV;

if(@ARGV!=3) {
    print STDERR "\nUSAGE ::: <proc #>  <batch #>  <biCorpus.batch.proc>\n";
    exit(-1);
}

#############################################################################################################################################

$l_max = 1000;
$l = 0;
@SentPair = ();
open(IN,"<$Bi");
open(OUT, ">biUnSegmentations.$batch.$proc");
while(<IN>) {
        chomp;
	s/\t//;

	@{ $SentPair[$l] } = split(/ XXX /);

	$l++;

	if ($l == $l_max) {
		for ($j=0; $j<$l_max; $j++) {
			print OUT "\t$SentPair[$j][0] XXX $SentPair[$j][1]\n";
			@unseg = ();
			for ($i=0; $i<=1; $i++) {
				@words = ();
				@words = split(/ /, $SentPair[$j][$i]);
				$unseg[$i] = join(' ||| ', @words);
			}
			print OUT "$unseg[0] XXX $unseg[1] XXX 1\n";
		}
		@SentPair = ();
		$l = 0;
	}
}
close(IN); 
if (scalar(@SentPair) > 0) {
	for ($j=0; $j<scalar(@SentPair); $j++) {
		print OUT "\t$SentPair[$j][0] XXX $SentPair[$j][1]\n";
		@unseg = ();
		for ($i=0; $i<=1; $i++) {
			@words = ();
			@words = split(/ /, $SentPair[$j][$i]);
			$unseg[$i] = join(' ||| ', @words);
		}
		print OUT "$unseg[0] XXX $unseg[1] XXX 1\n";	
	}
}
close(OUT);

system("touch _WORK_.$proc");
