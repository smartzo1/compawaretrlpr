#!/usr/bin/perl -w

($table, $S, $T, $NoF) = @ARGV;

if(@ARGV!=4) {
    print STDERR "\nUSAGE ::: <phrase-table>  <src lanf suffix>  <trg lang suffix>  <# features>\n";
    exit(-1);
}
#####################################################################################################

$l  = "$S".'2'."$T";
$_l = "$T".'2'."$S";

system("mv  $table $table.$l");

@store = ();
$counter = 0;
$Max = 1000000;

open(IN,"<$table.$l");
open(OUT, ">$table.$_l");
while(<IN>) {
	chomp;
	($s, $t, $f) = split(/ \|\|\| /);
	@F = ();
	@F = split(/ /, $f);
	@rest = ();
	@rest = @F[2*$NoF..scalar(@F)-1];
	if ($NoF == 2) {
		push(@store, "$t ||| $s ||| $F[2] $F[3] $F[0] $F[1] @rest");
	}
	elsif ($NoF == 1) {
		push(@store, "$t ||| $s ||| $F[1] $F[0] @rest");
        }
	if ($counter == $Max) {
		for ($i=0; $i<scalar(@store); $i++) { print OUT "$store[$i]\n"; }
		@store = ();
		$counter = -1;
	}
	$counter++;
}
close(IN);
if (scalar(@store) > 0) {
	for ($i=0; $i<scalar(@store); $i++) { print OUT "$store[$i]\n"; }
}
close(OUT);

system("sort -T . $table.$_l  -o  $table.$_l");

