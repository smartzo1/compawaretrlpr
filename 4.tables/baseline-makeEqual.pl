#!/usr/bin/perl -w

($Table, $Base, $dir) = @ARGV;

if(@ARGV!=3) {
    print STDERR "\n::: USAGE :::\n\n1) phrase-table.blabla\n2) baseline phrase/reorder table\n3) dir name for equality\n\n";
    exit(-1);
}
use POSIX;

open(IN1, "<$Table");
open(IN2, "<$Base");
open(OUT, ">$dir/$Base");
while($ltable=<IN1>, $lbase=<IN2>) {
	chomp($ltable);
	@T = ();
	@T = split(/ \|\|\| /, $ltable);
	@w = ();
	@w = split(/ /, $T[2]);
	if (($w[0] == 0) && ($w[2] == 0)) { next; }
	print OUT "$lbase";
}
close(IN1);
close(IN2);
close(OUT);

