#!/usr/bin/perl -w

($Table, $dir) = @ARGV;

if(@ARGV!=2) {
    print STDERR "\n::: USAGE :::\n\n1) phrase-table.blabla\n2) dir name for equality\n\n";
    exit(-1);
}
use POSIX;


open(IN, "<$Table");
open(OUT, ">$dir/$Table");
while(<IN>) {
	chomp;
	@T = ();
	@T = split(/ \|\|\| /);
	@w = ();
	@w = split(/ /, $T[2]);
	if (($w[0] == 0) && ($w[2] == 0)) { next; }
	print OUT "$_\n";
}
close(IN);
close(OUT);

