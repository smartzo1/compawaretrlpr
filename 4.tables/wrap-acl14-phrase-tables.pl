#!/usr/bin/perl -w

($F1, $F1name, $F2, $F2name, $Q, $M, $packets, $nops, $src, $trg) =@ARGV;

if(@ARGV!=10) {
    print STDERR "\n::: USAGE :::\n\n1) <featureWeights1 (DIR)>\n2) <name fw1>\n3) <featureWeights2 (DIR)>\n4) <name fw2>\n5) <counts/uniform (0/1)>\n6) <multipleLex>\n7) <# packets>\n8) <# procs>\n9) <src language suffix>\n10) <trg language suffix>\n\n";
    exit(-1);
}
use POSIX;
############################################################################################################################################################

$packet_size = floor( $nops / $packets );
@handler=();
for ($k=0; $k<$packets; $k++ ) {
        $handler[$k][0] = $k*$packet_size+1;
        $handler[$k][1] = ($k+1)*$packet_size;
}
$handler[0][0]=0;
$handler[$packets-1][1] = $nops;

for ($k=0; $k<$packets; $k++) {
        for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
		$F1file  = "$F1/$F1.$job";
		$F2file  = "$F2/$F2.$job";
		$Mulfile = "$M/$M.$job";
		system("nohup ./compute-phrase-tables.pl   $job  $F1file  $F1name  $F2file  $F2name  $Q   $Mulfile   1>log.$job  2>err.$job  &");
	}
	flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
}

############################################################################################################################################################

if ($F2name eq 'lex') { $MaxK = 1; }
else		      { $MaxK = 0; }

for ($k=0; $k<=$MaxK; $k++) {

	if ($k == 0) { 
		$ID  = "$F1name.$F2name.$Q"; 
		$NoF = 2; 
	}
	else { 
		$ID = "$F1name.$Q";
		$NoF = 1;      
	}

	system("cat  u-phrase-table.$ID.*  >>  u-phrase-table.$ID");

	system("rm   u-phrase-table.$ID.*");

	system("cat  m-phrase-table.$ID.*  >>  m-phrase-table.$ID");

	system("rm   m-phrase-table.$ID.*");

	system("sort -T . -u  m-phrase-table.$ID  -o  m-phrase-table.$ID");

	system("cat   u-phrase-table.$ID   m-phrase-table.$ID  >>  phrase-table.$ID");
	system("rm    u-phrase-table.$ID   m-phrase-table.$ID");
	system("sort  -T .  phrase-table.$ID  -o  phrase-table.$ID");

	system("./remove_duplicates.pl  phrase-table.$ID");

	system("./change-direction-phrase-table.pl   phrase-table.$ID   $src   $trg   $NoF");
}

############################################################################################################################################################

sub flagging {

my      ($start, $finish, $name)=@_;
my      $job;
my      @flag_job=();
my      $flags=0;
my      $NoJ;

        $NoJ = $finish - $start + 1;

        for ($job=$start; $job <= $finish; $job++) { $flag_job[$job]=0; }

        while($flags != $NoJ) {
                for ($job=$start; $job <= $finish; $job++) {
                        if (-e "$name.$job") { $flag_job[$job]=1;}
                }
                $flags=0;
                for ($job=$start; $job <= $finish; $job++) {
                        $flags += $flag_job[$job];
                }
		sleep(1);
        }
        for ($job=$start; $job <= $finish; $job++) { qx(rm $name.$job);  }
        return();
}
