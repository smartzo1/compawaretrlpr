#!/usr/bin/perl -w

($Base, $Table, $dir) = @ARGV;

if(@ARGV!=3) {
    print STDERR "\n::: USAGE :::\n\n1) baseline phrase/reorder table\n2) phrase-table.blabla\n3) dir name for equality\n\n";
    exit(-1);
}
use POSIX;

open(BA, "<$Base");
open(TL, "<$Table");
open(OUT, ">$dir/$Base");
while($lbase=<BA>, $ltable=<TL>) {
	chomp($lbase);
	chomp($ltable);
	@T = ();
	@T = split(/ \|\|\| /, $ltable);
	@w = ();
	@w = split(/ /, $T[2]);
	if (($w[1] == 0) && ($w[3] == 0)) { next; }
	print OUT "$lbase\n";
}
close(BA);
close(TL);
close(OUT);

