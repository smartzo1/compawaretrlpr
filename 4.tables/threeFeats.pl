#!/usr/bin/perl -w

($Base, $Table, $dir) = @ARGV;

if(@ARGV!=3) {
    print STDERR "\n::: USAGE :::\n\n1) baseline phrase table\n2) phrase-table.exact\n3) x2y\n\n";
    exit(-1);
}
use POSIX;

open(IN1, "<$Base");
open(IN2, "<$Table");
open(OUT, ">phrase-table.emp.lex.exact.1.$dir");
while($lbase=<IN1>, $ltable=<IN2>) {
	chomp($lbase);
	chomp($ltable);

        @B = ();
        @B = split(/ \|\|\| /, $lbase);
        @w_B = ();
        @w_B = split(/ /, $B[2]);

	@T = ();
	@T = split(/ \|\|\| /, $ltable);
	@w_T = ();
	@w_T = split(/ /, $T[2]);

	$pair = ();
	$pair = "$T[0] ||| $T[1]";

	print OUT "$pair ||| $w_B[0] $w_B[1] $w_T[0] $w_B[2] $w_B[3] $w_T[1] 2.718\n";
}
close(IN1);
close(IN2);
close(OUT);

