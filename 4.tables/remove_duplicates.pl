#!/usr/bin/perl -w

($table)=@ARGV;

if(@ARGV!=1) {
    print STDERR "\nUSAGE ::: <phrase table>\n";
    exit(-1);
}
use POSIX;

#######################################################################

$prev_pair = 'NULL';

open(IN,"<$table");
open(OUT,">new-$table");
while(<IN>) {
	chomp;
	($s, $t, $new_info) = split(/ \|\|\| /);
	$new_pair = "$s ||| $t";

	if ($new_pair ne $prev_pair) {
		if ($prev_pair ne 'NULL') {
			print OUT "$prev_pair ||| $prev_info\n";
		}
	}

	$prev_pair = $new_pair;
	$prev_info = $new_info;
}
close(IN);
print OUT "$prev_pair ||| $prev_info\n";
close(OUT);

system("rm  $table");
system("mv  new-$table  $table");

