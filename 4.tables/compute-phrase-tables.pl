#!/usr/bin/perl -w

($proc, $F1file, $F1name, $F2file, $F2name, $Q, $Multfile)=@ARGV;

if(@ARGV!=7) {
    print STDERR "\nUSAGE ::: <proc #>  <featureWeight1.proc>  <name fw1>  <featureWeight2.proc>  <name fw2>  <counts/uniform (0/1)>  <multipleLex.proc>\n";
    exit(-1);
}
############################################################################################################################################################

%M=();
open(IN,"<$Multfile");
while(<IN>) {
	chomp;
        @P = ();
        @P = split(/ \|\|\| /);
	$M{"$P[0] ||| $P[1]"} = 'def';
}
close(IN);

############################################################################################################################################################

open(FW1, "<$F1file");
open(FW2, "<$F2file");

$ID = "$F1name.$F2name.$Q";
open(MOUT,   ">m-phrase-table.$ID.$proc");
open(UOUT,   ">u-phrase-table.$ID.$proc");
$MaxJ = 0;

if ($F2name eq 'lex') {
	$ID = "$F1name.$Q";
	open(MNOLEX, ">m-phrase-table.$ID.$proc");
	open(UNOLEX, ">u-phrase-table.$ID.$proc");
	$MaxJ = 1;
}

%store = ();
($t_line, $l_line) = ('V', 'V');
$read = 1;

while ($t_line and $l_line) {
	
	$t_line = read_line('FW1');
	if (!defined($t_line)) { last; }
	@T = ();
	@T = split(/ \|\|\| /, $t_line);
	@p = ();
	@p = split(/ /, $T[2]);

	if ($read == 1) {
		$l_line = read_line('FW2');
		@L = ();
		@L = split(/ \|\|\| /, $l_line);
		@w = ();
		@w = split(/ /, $L[2]);
	}

	if ( ($T[0] ne $L[0]) || ($T[1] ne $L[1]) ) { 
		print "$proc) Something is horribly wrong!\n"; 
		print "$T[0] ||| $T[1] ----- $L[0] ||| $L[1]\n\n";
		#die; 
		$read = 0;
		next;
	}
	else {		
		$read = 1;
	}

	@FH = ();
	if (defined($M{"$T[0] ||| $T[1]"})) { @FH = ('MOUT', 'MNOLEX'); }
	else 				    { @FH = ('UOUT', 'UNOLEX'); }

	@{ $store{"$T[0] ||| $T[1]"}[0] } = @FH;
	@{ $store{"$T[0] ||| $T[1]"}[1] } = ("$p[0] $w[0] $p[1] $w[1]", "$p[0] $p[1]");
	
	if (scalar(keys(%store)) == 1000) {
		foreach $pair (keys(%store)) {
			for ($j=0; $j<=$MaxJ; $j++) {
				$H = $store{$pair}[0][$j];
				$weights = $store{$pair}[1][$j];
				print $H "$pair ||| $weights 2.718\n";
			}
		}
		%store = ();
	} 
}

if (scalar(keys(%store)) != 0) {
	foreach $pair (keys(%store)) {
		for ($j=0; $j<=$MaxJ; $j++) {
	        	$H = $store{$pair}[0][$j];
        	        $weights = $store{$pair}[1][$j];
                	print $H "$pair ||| $weights 2.718\n";
		}
	}
}

close(FW1); 
close(FW2);

close(MOUT); 
close(UOUT);
if ($MaxJ == 1) {
	close(MNOLEX);
	close(UNOLEX);
}

###########################################################################

system("touch _WORK_.$proc");

###########################################################################

sub read_line {

my      ($FH) = @_;
my      $line;

        if ($FH and $line = <$FH>) {
                chomp($line);
                return($line);
        }
        return;
}

