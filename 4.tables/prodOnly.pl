#!/usr/bin/perl -w

($Table, $dir) = @ARGV;

if(@ARGV!=2) {
    print STDERR "\n::: USAGE :::\n\n1) phrase-table.blabla\n2) x2y\n\n";
    exit(-1);
}
use POSIX;


open(IN, "<$Table");
open(OUT, ">phrase-table.prod.1.$dir");
while(<IN>) {
	chomp;
	@T = ();
	@T = split(/ \|\|\| /);
	@w = ();
	@w = split(/ /, $T[2]);
	if (($w[0] == 0) && ($w[2] == 0)) { next; }
	print OUT "$T[0] ||| $T[1] ||| $w[1] $w[3] 2.718\n";
}
close(IN);
close(OUT);

