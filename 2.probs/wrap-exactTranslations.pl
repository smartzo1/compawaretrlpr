#!/usr/bin/perl -w

($P, $U, $G, $packets, $nops, $NoP, $MaxNoComps)=@ARGV;

if(@ARGV!=7) {
    print STDERR "\n::: USAGE :::\n\n1) <continuous.phrase.0 (DIR)>\n2) <phrasepairs.cont.0 (DIR)>\n3) <phrasepairs.disc.0 (DIR)>\n4) <# packets>\n5) <# procs>\n6) <n pairs for P(n)>\n7) <Maximum # components>\n\n";
    exit(-1);
}
use POSIX;
###########################################################################################################################################################

@handler=();
$packet_size = floor( $nops / $packets );
for ($k=0; $k<$packets; $k++ ) {
        $handler[$k][0] = $k*$packet_size+1;
        $handler[$k][1] = ($k+1)*$packet_size;
}
$handler[0][0] = 0;
$handler[$packets-1][1] = $nops;

###########################################################################################################################################################

for ($k=0; $k<$packets; $k++) {
        for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
	#for ($job=0; $job<=19; $job++) {
		$pairs  = "$P/$P.$job";
		$unions = "$U/$U.$job";
		$gappy  = "$G/$G.$job";
                system("nohup ./compute-exactTranslations.pl  $job  $pairs  $NoP  $unions  $gappy  $MaxNoComps  1>log.$job  2>>err.$job  &");
        }
        #flagging(0, 19,  "_WORK_");
	flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
}

for ($Q=0; $Q<=1; $Q++) {
	system("mkdir  exactProbabilities.$Q");
	system("mv     exactProbabilities.$Q.*   exactProbabilities.$Q");
}

###########################################################################################################################################################

system("touch  DONE.EXACT");

###########################################################################################################################################################

sub flagging {

my      ($start, $finish, $name)=@_;
my      $job;
my      @flag_job=();
my      $flags=0;
my      $NoJ;

        $NoJ = $finish - $start + 1;

        for ($job=$start; $job <= $finish; $job++) { $flag_job[$job]=0; }

        while($flags != $NoJ) {
                for ($job=$start; $job <= $finish; $job++) {
                        if (-e "$name.$job") { $flag_job[$job]=1;}
                }
                $flags=0;
                for ($job=$start; $job <= $finish; $job++) {
                        $flags += $flag_job[$job];
                }
        }
        for ($job=$start; $job <= $finish; $job++) { qx(rm $name.$job);  }
        return();
}


