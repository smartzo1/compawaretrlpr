#!/usr/bin/perl -w 

($proc, $allUnionsFile, $gappyUnionsFile) = @ARGV;

if(@ARGV!=3) {
   print STDERR "\n::: USAGE ::: <proc #>   <phrasepairs.cont.batch.proc>  <phrasepairs.disc.batch.proc>\n\n";
   exit(-1);
}

#############################################################################################################################################################

open(IN, "<$gappyUnionsFile");
while(<IN>) {
	chomp;
	@P = ();
	@P = split(/ \|\|\| /);
	for ($i=0; $i<=1; $i++) {
		if ($P[$i] =~ /GAP/) { next; }
		$H[$i]{$P[$i]} = 'UNDEF';
	}
}
close(IN);

open(IN, "<$allUnionsFile");
while(<IN>) {
        chomp;
        @P = ();
        @P = split(/ \|\|\| /);
	for ($i=0; $i<=1; $i++) {
		if (!defined($H[$i]{$P[$i]})) { next; }
		@c = ();
		@c = split(/ /, $P[2]);
		$H[$i]{$P[$i]} = $c[$i+1];
	}
}
close(IN);

open(IN, "<$gappyUnionsFile");
open(OUT, ">temp.updated.disc.$proc");
while(<IN>) {
        chomp;
        @P = ();
        @P = split(/ \|\|\| /);
	@c = ();
	@c = split(/ /, $P[2]);
	@new = ();
	$new[0] = $c[0];
        for ($i=0; $i<=1; $i++) {
		if (!defined($H[$i]{$P[$i]})) {
			$new[$i+1] = $c[$i+1];
		}
		else {
			if ($H[$i]{$P[$i]} eq 'UNDEF') {
				$new[$i+1] = $c[$i+1];
			}
			else {
				$new[$i+1] = $H[$i]{$P[$i]};
			}
		}
	}
	print OUT "$P[0] ||| $P[1] ||| @new\n";
}
close(IN);
close(OUT);

system("rm  $gappyUnionsFile");
system("mv  temp.updated.disc.$proc  $gappyUnionsFile");

system("touch  _WORK_.$proc");

