#!/usr/bin/perl -w

($proc, $C)=@ARGV;

if(@ARGV!=2) {
    print STDERR "\nUSAGE ::: <proc #>  <id.phrase.0 (DIR)>\n";
    exit(-1);
}
################################################################

$file = "$C/$C.$proc";

open(IN, "<$file");
open(OUT, ">temp.cont.phrase.0.$proc");
while(<IN>) {
	chomp;

        @P = ();
        @P = split(/ \|\|\| /);

	@word2pos = ();
	for ($i=0; $i<=1; $i++) {
		@tokens = ();
		@tokens = split(/ /, $P[$i+1]);
		for ($j=0; $j<scalar(@tokens); $j++) {
			$word2pos[$i]{$tokens[$j]} = $j;
		}
		$word2pos[$i]{'NULL'} = 'NULL';	
		$word2pos[$i]{'GAP'} = 'GAP';
	}

	@comps = ();
	@comps = @P[3..scalar(@P)-1];
	@l_comps = ();
	for ($n=0; $n<scalar(@comps); $n++) {
		@cc = ();
		@cc = split(/ XXX /, $comps[$n]);
		@list = ();
		@l_cc = ();
		for ($i=0; $i<=1; $i++) {
			@tokens = ();
			@tokens = split(/ /, $cc[$i]);
			for ($j=0; $j<scalar(@tokens); $j++) {
				push( @{ $list[$i] }, $word2pos[$i]{$tokens[$j]} );
			}
			$l_cc[$i] = join(' ', @{ $list[$i] });
		}
		$ref = ();
		$ref = join(' XXX ', @l_cc);
		push( @l_comps, $ref);
	}

	$o = join(' ||| ', (@P[1..scalar(@P)-1], @l_comps) );

	$o =~ s/\#\d+//g; 

	print OUT "$o ||| 1\n";
	
}
close(IN);
close(OUT);

system("touch  _WORK_.$proc");
	

