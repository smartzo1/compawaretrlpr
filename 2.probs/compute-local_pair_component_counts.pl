#!/usr/bin/perl -w

($proc, $C)=@ARGV;

if(@ARGV!=2) {
    print STDERR "\nUSAGE ::: <proc #>  <counts (DIR)>\n";
    exit(-1);
}
################################################################

$file = "$C/$C.$proc";

system("sort  -T .  -t'|'  -k1,1  $file  -o  $file");

$prev_pair = 'VOID';
@accum_count = ();
%CC = ();
%LCC = ();
open(IN, "<$file");
open(OUT, ">temporary.$proc");
while(<IN>) {
	chomp;
	
	@P = ();
	@P = split(/ \|\|\| /);
	@count = ();
	@count = split(/ /, $P[scalar(@P)-1]);

	@info  = (); 
	@temp  = (); @stemp  = (); $comps = ();
	@ltemp = (); $lcomps = ();
	
	@info = @P[2..scalar(@P)-2];
	$n = scalar(@info);

	@temp   = @info[0..($n/2)-1];
	@stemp  = sort(@temp);
	$comps  = join(' ||| ', @stemp);
	@ltemp  = @info[$n/2..$n-1];
	$lcomps = join(' ||| ', @ltemp);
 
	if ($prev_pair eq 'VOID') { 
		$prev_pair = "$P[0] ||| $P[1]";
		$LCC{$comps} = $lcomps; 
		for ($k=0; $k<scalar(@count); $k++) { 
			$accum_count[$k] = $count[$k]; 
			$CC{$comps}[$k]  = $count[$k];
		}
		next; 
	}

	$pair = "$P[0] ||| $P[1]";
	if ($prev_pair eq $pair) {
		$LCC{$comps} = $lcomps;
                for ($k=0; $k<scalar(@count); $k++) { 
			$accum_count[$k] += $count[$k];
			$CC{$comps}[$k] += $count[$k];
		}
                next;
	}

	foreach $cc (keys(%CC)) {
		print OUT "$prev_pair ||| $cc ||| $LCC{$cc} ||| @{ $CC{$cc} } @accum_count\n";
	}
	$prev_pair = $pair;
	@accum_count = ();
	%CC = ();
	%LCC = ();
	$LCC{$comps} = $lcomps;
	for ($k=0; $k<scalar(@count); $k++) { 
		$accum_count[$k] = $count[$k]; 
		$CC{$comps}[$k] += $count[$k];
	}
}
close(IN);
foreach $cc (keys(%CC)) {
	print OUT "$prev_pair ||| $cc ||| $LCC{$cc} ||| @{ $CC{$cc} } @accum_count\n";
}
close(OUT);

system("rm  $file");
system("mv  temporary.$proc  $file");

system("touch  _WORK_.$proc");
	

