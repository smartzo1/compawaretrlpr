#!/usr/bin/perl -w

($proc, $C, $MissingFile)=@ARGV;

if(@ARGV!=3) {
    print STDERR "\nUSAGE ::: <proc #>   <comps.phrase.0 (DIR)>  <missingCocos.proc>\n";
    exit(-1);
}
###############################################################################################################

%L = ();
open(IN, "<$MissingFile");
while(<IN>) {
	chomp;
	($s, $t, $n) = split(/ \|\|\| /);
	$L{"$s ||| $t"} = $n;
}
close(IN);

$file = "$C/$C.$proc";

open(OUT, ">>$file");
foreach $cc (keys(%L)) {
	print OUT "$cc ||| $L{$cc}\n";
}
close(OUT);	

system("rm  $MissingFile");
system("sort -T .  $file  -o  $file");

system("touch  _WORK_.$proc");
 
