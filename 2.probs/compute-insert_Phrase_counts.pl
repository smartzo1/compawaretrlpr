#!/usr/bin/perl -w

($proc, $C) = @ARGV;

if(@ARGV!=2) {
    print STDERR "\nUSAGE ::: <proc #>   <weighted counts (DIR)>\n";
    exit(-1);
}
########################################################################################################

@label = ('src', 'trg');

system("mv  $C/$C.$proc   temporaryPair.0.$proc"); 
for ($i=0; $i<=1; $i++) {
	%H = ();
	$dir = $label[$i].'PhraseCount';
	open(IN, "<$dir/$dir.$proc");
	while(<IN>) {
		chomp;
		@P = ();
		@P = split(/ \|\|\| /);
		@c = ();
		@c = split(/ /, $P[1]);
		@{ $H{$P[0]} } = @c;
	}
	close(IN);
	$j = $i+1;
	open(IN, "<temporaryPair.$i.$proc");
	open(OUT, ">temporaryPair.$j.$proc");
	while(<IN>) {
		chomp;
		@P = split(/ \|\|\| /);
		print OUT "$_ @{$H{$P[$i]}}\n";
	}
	close(IN);
	system("rm  temporaryPair.$i.$proc");
}
system("mv  temporaryPair.2.$proc  $C/$C.$proc");

system("touch  _WORK_.$proc");

