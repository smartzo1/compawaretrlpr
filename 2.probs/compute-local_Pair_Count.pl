#!/usr/bin/perl -w

($proc, $C)=@ARGV;

if(@ARGV!=2) {
    print STDERR "\nUSAGE ::: <proc #>  <counts (DIR)>\n";
    exit(-1);
}
################################################################

$file = "$C/$C.$proc";

system("sort  -T .  -t'|'  -k1,1  $file  -o  $file");

$prev_pair = 'VOID';
@accum_count = ();
open(IN, "<$file");
open(OUT, ">temporary.$proc");
while(<IN>) {
	chomp;
	
	@P = ();
	@P = split(/ \|\|\| /);
	@count = ();
	@count = split(/ /, $P[2]);

	if ($prev_pair eq 'VOID') { 
		$prev_pair = "$P[0] ||| $P[1]";
		for ($k=0; $k<scalar(@count); $k++) { $accum_count[$k] = $count[$k]; }
		next; 
	}

	$pair = "$P[0] ||| $P[1]";
	if ($prev_pair eq $pair) {
                for ($k=0; $k<scalar(@count); $k++) { $accum_count[$k] += $count[$k]; }
                next;
	}

	print OUT "$prev_pair ||| @accum_count\n";
	$prev_pair = $pair;
	@accum_count = ();
	for ($k=0; $k<scalar(@count); $k++) { $accum_count[$k] = $count[$k]; }
}
close(IN);
print OUT "$prev_pair ||| @accum_count\n";
close(OUT);

system("rm  $file");
system("mv  temporary.$proc  $file");
system("sort  -T .  -t'|'  -k4,4  $file  >  $C/trg$C.$proc");

system("touch  _WORK_.$proc");
	

