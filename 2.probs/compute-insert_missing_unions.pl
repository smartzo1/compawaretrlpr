#!/usr/bin/perl -w 

use strict;

my ($proc, $allUnions, $gappyUnions, $missingFile, $NoP, $nops) = @ARGV;

if(@ARGV!=6) {
   print STDERR "\nUSAGE ::: <proc #>  <phrasepairs.cont.0 (DIR)>  <phrasepairs.disc.0 (DIR)>  <missingFile.proc>  <n pairs for P(n)>  <# procs>\n";
   exit(-1);
}

#############################################################################################################################################################

my @W = ();
my $counter = 1;
my @_UC_ = ();
my @_GC_ = ();

open(IN, "<$missingFile");
while(<IN>) {
	chomp;
	push( @W, $_); 
	if ($counter % $NoP == 0) {
		storeInMemory(\@W, \@_UC_, \@_GC_);
		findCounts(\@_UC_, $allUnions, $nops, "cont.$proc");
		findCounts(\@_GC_, $gappyUnions, $nops, "disc.$proc");
		@W = ();
		@_UC_ = ();
		@_GC_ = ();
	}
	$counter++;
}
close(IN);
storeInMemory(\@W, \@_UC_, \@_GC_);
findCounts(\@_UC_, $allUnions, $nops, "cont.$proc");
findCounts(\@_GC_, $gappyUnions, $nops, "disc.$proc");

#########################################################################################################################

system("rm   $missingFile");
system("sort -T .  -u  updated.missing.cont.$proc -o  updated.missing.cont.$proc");
system("sort -T .  -u  updated.missing.disc.$proc -o  updated.missing.disc.$proc");

system("touch  _WORK_.$proc");
	
#########################################################################################################################

sub findCounts {

my	($_D_, $dirName, $nops, $type) = @_;

my	($i, $u, $file);
my      @P;

	open(OUT, ">>updated.missing.$type");
	for ($i=0; $i<=$nops; $i++) {
		if (!defined($$_D_[$i])) { next; }
		$file = "$dirName/$dirName.$i";
	        open(FIN, "<$file");
        	while(<FIN>) {
                	chomp;
	                @P = ();
        	        @P = split(/ \|\|\| /);
                	$u = "$P[0] ||| $P[1]";
	                if (!defined($$_D_[$i]{$u})) { next; }
			print OUT "$u ||| $P[2]\n";
			delete($$_D_[$i]{$u});
			if (scalar(keys(%{$$_D_[$i]})) == 0 ) { last; }
        	}
	        close(FIN);
	}
	close(OUT);
}

#########################################################################################################################

sub storeInMemory {

my	($W, $_UC_, $_GC_) = @_;

my	($i, $ID, $temp, $n);
my	@data;
my	@struct;
my	@P;
my	@L;
my	@O;
my	@pos2word;

	for ($i=0; $i<scalar(@$W); $i++) {

		@data = ();
		@data = split(/ \|\|\| /, $$W[$i]);
		$ID = ();
		$ID = $data[0];
		@P = ();
		@P = @data[1..2];
		$n = scalar(@data);
		@struct = ();
		@struct = @data[3..$n-1];
		$n = scalar(@struct); 
		@L = ();
        	@L = @struct[$n/2..$n-1];
		@pos2word = ();

		wordDataStruct(\@P, \@pos2word);
		allSubPairs(\@L, \@pos2word, $_UC_, $_GC_, $ID); 
	}
}

#########################################################################################################################

sub allSubPairs {

my	($L, $pos2word, $_UC_, $_GC_, $ID) = @_;

my	($n, $j, $full_string, $string, $k, $bit, $i, $l, $NoC, $flag);

my	@comp;
my	@loc_labels;
my	@labels;
my      @CheckBothNULL;

        $n = scalar(@$L);
        for ($j=1; $j<2**$n; $j++) {
                $full_string = dec2bin($j);
                $string = substr($full_string, -$n, $n );
		@labels = ();
		$NoC = 0;
                @CheckBothNULL = ();
                $flag = 1;
                for ($k=0; $k<$n; $k++) {
                        $bit = substr($string, $k, 1);
			$NoC += $bit;
                        if ($bit eq "0") { next; }
			push( @CheckBothNULL, $$L[$k] );
			@comp = ();
			@comp = split(/ XXX /, $$L[$k]);
			for ($i=0; $i<=1; $i++) {
				$comp[$i] =~ s/GAP //g;
				@loc_labels = ();
				@loc_labels = split(/ /, $comp[$i]);
				for ($l=0; $l<scalar(@loc_labels); $l++) {
					if ($loc_labels[$l] eq 'NULL') {
						$loc_labels[$l] = ();
						$loc_labels[$l] = 99;
					}
				}
				push( @{ $labels[$i] }, @loc_labels);
			}		
                }

		if ($NoC > 1) { $flag = checkUnaligned(\@CheckBothNULL); }

		if ($flag == 0) { next; }

		labels2pair(\@labels, $pos2word, $_UC_, $_GC_, $ID);
        }
}

#########################################################################################################################

sub labels2pair {

my	($labels, $pos2word, $_UC_, $_GC_, $ID) = @_;

my	($i, $j, $w, $diff, $g);
my	@sorted_labels = ();
my	@list_pair = ();
my	@pair = ();
my	$find = ();
my	@dummy = ();

	for ($i=0; $i<=1; $i++) {
		@{ $sorted_labels[$i] } = sort { $a <=> $b } @{ $$labels[$i] };
		$w = $$pos2word[$i]{$sorted_labels[$i][0]};
		push( @{ $list_pair[$i] }, $w);
		if ($w eq 'NULL') { next; }
		for ($j=1; $j<scalar(@{ $sorted_labels[$i] }); $j++) {
			$w = $$pos2word[$i]{$sorted_labels[$i][$j]};
			if ($w eq 'NULL') { last; }
			$diff = $sorted_labels[$i][$j] - $sorted_labels[$i][$j-1];

                        if ($diff != 1) {
                                @dummy = ();
                                for ($g=1; $g<$diff; $g++) {
                                        push(@dummy , 'GAP');
                                }
                                push(@dummy, $w);
                                $w = ();
                                $w = join(' ', @dummy);
                        }

			push( @{ $list_pair[$i] }, $w);
		}
	}
	for ($i=0; $i<=1; $i++) { $pair[$i] = join(' ', @{ $list_pair[$i] }); }

	$find = join(' ||| ', @pair);

	if ($find !~ /GAP/) {
		$$_UC_[$ID]{$find} = 'def';
	}
	else {
                $$_GC_[$ID]{$find} = 'def';
	}
}

#########################################################################################################################

sub dec2bin { return unpack("B32", pack("N", shift)); }

#########################################################################################################################

sub wordDataStruct {

my      ($P, $pos2word) = @_;

my      ($i, $j);
my      @tokens;

        for ($i=0; $i<=1; $i++) {
                @tokens = ();
                @tokens = split(/ /, $$P[$i]);
                for ($j=0; $j<scalar(@tokens); $j++) {
                        $$pos2word[$i]{$j} = $tokens[$j];
                }
                $$pos2word[$i]{99} = 'NULL';
        }
}

#########################################################################################################################

sub checkUnaligned {

my      ($CheckBothNULL) = @_;

my      ($i, $j, $k);
my      @comp;
my      $flag = 0;
my      @sideNull = (0,0);

        for ($k=0; $k<scalar(@$CheckBothNULL); $k++) {
                @comp = ();
                @comp = split(/ XXX /, $$CheckBothNULL[$k]);
                for ($i=0; $i<=1; $i++) {
                        if ($comp[$i] eq 'NULL') {
                                $flag++;
                                $sideNull[$i] = 1;
                        }
                }
        }

        if ($flag == scalar(@$CheckBothNULL)) {
                if ($sideNull[0] + $sideNull[1] == 1) { return(1); }
                else                                  { return(0); }
        }
        else {
                return(1);
        }
}

