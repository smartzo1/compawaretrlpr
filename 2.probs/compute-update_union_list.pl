#!/usr/bin/perl -w

($proc, $U, $MissingFile)=@ARGV;

if(@ARGV!=3) {
    print STDERR "\nUSAGE ::: <proc #>   <allUnions.phrase.0 (DIR)>  <updated.missingUnions.proc>\n";
    exit(-1);
}
###############################################################################################################

$file = "$U/$U.$proc";

system("cat  $MissingFile  >>  $file");

system("rm   $MissingFile");

system("sort -T .  -u  $file  -o  $file");

system("touch  _WORK_.$proc");
 
