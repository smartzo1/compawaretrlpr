#!/usr/bin/perl -w

($U, $G, $ID, $packets, $nops, $NoP, $MaxNoC)=@ARGV;

if(@ARGV!=7) {
    print STDERR "\n::: USAGE :::\n\n1) <phrasepairs.cont.0 (DIR)>\n\n2) <phrasepairs.disc.0 (DIR)>\n\n3) <id.phrase.0 (DIR)>\n\n4) <# packets>\n\n5) <# procs>\n\n6) <Maximum # pairs for PowerSet(pair) to store in memory>\n\n7) <Maximum # components>\n\n\n";
    exit(-1);
}
use POSIX;

############################################################################################################################################

$NoMeas = 1;

############################################################################################################################################
############################################################################################################################################
=pod
@C = ();
@C = ($U, $G);
@log = ();
@log = ('cont', 'disc');

for ($i=0; $i<=1; $i++) {

	system("nohup ./wrap-translation_probabilities.pl  $C[$i]  $packets  $NoMeas  $nops  $log[$i]  &");

	while(!(-e "DONE.EMPTP.$log[$i]")) { sleep(1); }
	system("rm  DONE.EMPTP.$log[$i]");

}

############################################################################################################################################

system("nohup ./wrap-collect_continuous_pairs.pl  $ID  $U  $G  $packets  $nops  $NoP  &");

while(!(-e 'DONE.UPDATE')) { sleep(1); }
system("rm  DONE.UPDATE");
=cut
############################################################################################################################################

system("nohup ./wrap-phrase2gap_update_side_counts.pl  $U  $G  $packets  $nops  &");

while(!(-e 'DONE.P2GUPD')) { sleep(1); }
system("rm  DONE.P2GUPD");

############################################################################################################################################

system("nohup ./wrap-exactTranslations.pl  continuous.phrase.0  $U  $G  $packets  $nops  $NoP  $MaxNoC  &");

while(!(-e 'DONE.EXACT')) { sleep(1); }
system("rm  DONE.EXACT");

############################################################################################################################################

