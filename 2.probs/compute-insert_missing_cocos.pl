#!/usr/bin/perl -w

($proc, $C, $MissingFile, $nops)=@ARGV;

if(@ARGV!=4) {
    print STDERR "\nUSAGE ::: <proc #>   <comps.phrase.0 (DIR)>  <missingCocos.proc>  <# procs>\n";
    exit(-1);
}
###############################################################################################################

@L = ();
open(IN, "<$MissingFile");
while(<IN>) {
	chomp;
	($i, $cc) = split(/ \|\|\| /);
	$L[$i]{$cc} = 'def';
}
close(IN);


for ($i=0; $i<=$nops; $i++) {
	if (!defined($L[$i])) { next; }
	$file = "$C/$C.$i";
	open(IN, "<$file");
	while(<IN>) {
		chomp;
		@P = ();
		@P = split(/ \|\|\| /);
		$cc = "$P[0] XXX $P[1]";
		if (!defined($L[$i]{$cc})) { next; }
		$L[$i]{$cc} = $P[2];
	}
	close(IN);
}

system("rm  $MissingFile");

open(OUT, ">$MissingFile");
for ($i=0; $i<=$nops; $i++) {
	if (!defined($L[$i])) { next; }
	foreach $cc (keys(%{$L[$i]})) {
		$x = $cc;
		$cc =~ s/XXX/\|\|\|/;
		print OUT "$cc ||| $L[$i]{$x}\n";
	}
}
close(OUT);

#system("sort -T . temporaryPhrase.$proc  -o  temporaryPhrase.$proc");

system("touch  _WORK_.$proc");
 


