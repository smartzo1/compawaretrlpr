#!/usr/bin/perl -w

($proc, $dir, $nops) = @ARGV;

if(@ARGV!=3) {
    print STDERR "\nUSAGE ::: <proc #>   <temp.cont.phrase.0>   <# procs>\n";
    exit(-1);
}
########################################################################################################

#
#
#   "                                        +                                                                    +                                             +    +"
#   "                                        +                                                                    +                                             +    +"
#   "                                        +                                                                    +                                             +    +"
#   "                                        +--------------------------------------------------------------------+---------------------------------------------+----+"
#   "                                        |                            |               |                       |           |              |                  |     "
#   "      0                   1             |         2                  |      3        |           4           |     5     |     6        |        7         |   8 "
#   "el periodo ||| session of the european ||| NULL XXX of the european ||| el XXX NULL ||| periodo XXX session ||| 1 XXX 0 ||| 0 XXX NULL ||| NULL XXX 1 2 3 ||| 1 1"
#
#

$file = "$dir/$dir.$proc";
%H = ();
%L = ();
open(IN, "<$file");
while(<IN>) {
	chomp;
	@P = ();
	@P = split(/ \|\|\| /);
	@c = ();
	@c = split(/ /, $P[scalar(@P)-1]);
	@info = ();
	@info = @P[2..scalar(@P)-2];
	$n = scalar(@info);	

	@temp = ();
	@temp = @info[0..($n/2)-1];
	foreach $cc (@temp) { $L{$cc} = 'def'; }
	$comps = ();
	$comps = join(' ||| ', @temp);

	@temp = ();
	@temp = @info[$n/2..$n-1];
	$ncomps = ();
	$ncomps = join(' ||| ', @temp);

	@{ $H{"$P[0] ||| $P[1]"}{$comps}[0] } = @c;
	   $H{"$P[0] ||| $P[1]"}{$comps}[1]   = $ncomps;

	$NoM = scalar(@c)/2;
}
close(IN);

#open(COUT, ">missingCocos.$proc");
open(UOUT, ">missingUnions.$proc");
for ($i=0; $i<=$nops; $i++) {
	if ($i == $proc) { next; }
	$_file = "$dir/$dir.$i";
	open(IN, "<$_file");
	while(<IN>) {
		chomp;
		@P = ();
		@P = split(/ \|\|\| /);
		if (!defined($H{"$P[0] ||| $P[1]"})) { next; }
		@c = ();
		@c = split(/ /, $P[scalar(@P)-1]);
		@info = ();
		@info = @P[2..scalar(@P)-2];
		$n = scalar(@info);

	        @temp  = ();
		@temp  = @info[0..($n/2)-1];
		#$NoC = scalar(@temp);
		foreach $cc (@temp) {
			if (!defined($L{$cc})) {
				#print COUT "$i ||| $cc\n";
				$L{$cc} = 'def';
			}
		}
		$comps = ();
		$comps = join(' ||| ', @temp);

                @temp = ();
                @temp = @info[$n/2..$n-1];
		$ncomps = ();
		$ncomps = join(' ||| ', @temp);

		$pair = "$P[0] ||| $P[1]";

		if (!defined($H{$pair}{$comps})) {
			@old_c = ();
			foreach $u (keys(%{$H{$pair}})) {
				if (scalar(@old_c) == 0) { @old_c = @{ $H{$pair}{$u}[0] }; }
				for ($k=scalar(@c)/2; $k<scalar(@c); $k++) {
					$H{$pair}{$u}[0][$k] += $c[$k-$NoM];
				}
			}
			for ($k=0; $k<scalar(@c)/2; $k++) {
				$H{$pair}{$comps}[0][$k] = $c[$k];
			}
			for ($k=scalar(@c)/2; $k<scalar(@c); $k++) {
				$H{$pair}{$comps}[0][$k] += $c[$k-$NoM]+$old_c[$k];
			}
			$H{$pair}{$comps}[1] = $ncomps;
			#if ($NoC > 1) {
				print UOUT "$i ||| $pair ||| $comps ||| $ncomps\n";
			#}
		}
		else {
			for ($k=0; $k<scalar(@c)/2; $k++) {
				$H{$pair}{$comps}[0][$k] += $c[$k];
			}
			foreach $u (keys(%{$H{$pair}})) {
				for ($k=scalar(@c)/2; $k<scalar(@c); $k++) {
					$H{$pair}{$u}[0][$k] += $c[$k-$NoM];
				}
			}
		}	
	}
	close(IN);
}
#close(COUT);
close(UOUT);

open(OUT, ">continuous.phrase.0.$proc");
foreach $pair (keys(%H)) {
	foreach $comps (keys(%{$H{$pair}})) {
		print OUT "$pair ||| $comps ||| $H{$pair}{$comps}[1] ||| @{ $H{$pair}{$comps}[0] }\n"; 
	}
}
close(OUT);

system("sort -T .  continuous.phrase.0.$proc  -o  continuous.phrase.0.$proc");

system("touch  _WORK_.$proc");
 


