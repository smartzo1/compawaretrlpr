#!/usr/bin/perl -w 

use strict;

my ($proc, $pairsFile, $NoP, $allUnionsFile, $gappyUnionsFile, $MaxNoComps) = @ARGV;

if(@ARGV!=6) {
   print STDERR "\n::: USAGE :::\n\n1) <proc #>\n2) <continuous.phrase.batch.proc>\n3) <n pairs for Power(n)>\n4) <phrasepairs.cont.batch.proc>\n5) <phrasepairs.disc.batch.proc>\n6) <Maximum # components>\n\n";
   exit(-1);
}

BEGIN { unshift @INC, qw( /home/smartzo1/Perl_spyros/Algorithm-Combinatorics-0.27/blib/lib); }
BEGIN { unshift @INC, qw( /home/smartzo1/Perl_spyros/Algorithm-Combinatorics-0.27/blib/arch/); }
#BEGIN { unshift @INC, qw( /home/spyros/Perl_spyros/Algorithm-Combinatorics-0.27/blib/arch/); }
#BEGIN { unshift @INC, qw( /home/spyros/Perl_spyros/Algorithm-Combinatorics-0.27/blib/lib); }
use Algorithm::Combinatorics qw(subsets permutations);

BEGIN { unshift @INC, qw( /home/smartzo1/Perl_spyros/List-Compare-0.38/lib/ ); }
#BEGIN { unshift @INC, qw( /home/spyros/Perl_spyros/List-Compare-0.37/lib); }
use List::Compare;

#############################################################################################################################################################

my $counter = 1;
my @P;
my @occurr;
my $N = 0;
my $nameDir = "toBeProcessed.$proc";

system("mkdir  $nameDir");

open(IN, "<$pairsFile");
open(OUT, ">$nameDir/$nameDir.$N");
while(<IN>) {
        chomp;
        @P = ();
        @P = split(/ \|\|\| /);
        @occurr = ();
        @occurr = split(/ /, $P[scalar(@P)-1]);
        if ($occurr[0] != $occurr[1]) { 
        	print OUT "$_\n";
		next; 
	}
        if ($counter % $NoP == 0) {
                close(OUT);
		$N++;
		open(OUT, ">$nameDir/$nameDir.$N");
        }
	print OUT "$_\n";
        $counter++;
}
close(IN);
close(OUT);

############################################################################################################################################################

my $M;
my %_UC_;
my %_GC_;
my $log_counter = 0;

for ($M=0; $M<=$N; $M++) {
	%_UC_ = ();
	%_GC_ = ();

	storePairsInMemory(\%_UC_, \%_GC_, $nameDir, $M);
	findPairCounts(\%_UC_, $allUnionsFile);
	findPairCounts(\%_GC_, $gappyUnionsFile);
	$log_counter = work(\%_UC_, \%_GC_, $proc, $log_counter, $nameDir, $M);
}

###########################################################################################################################################################

system("rm  -r  $nameDir");

my $Q;

for ($Q=0; $Q<=1; $Q++) {
	system("sort  -T .  exactProbabilities.$Q.$proc  -o  exactProbabilities.$Q.$proc");
}

system("touch  _WORK_.$proc");
	
#########################################################################################################################

sub work {

my	($_UC_, $_GC_, $proc, $log_counter, $nameDir, $M) = @_;

my	($line, $n, $pair, $p_G, $Q, $i, $MaxI);
my	@data;
my	@P;
my	@O;
my	@struct;
my	@L;
my	@null_L;
my	@pos2word;
my	@labelStruct;
my	%C2P;
my	@Probs;
my	%Output = ();

        open(TBP, "<$nameDir/$nameDir.$M");
        while($line=<TBP>) {
		
		chomp($line);
                @data = (); 
		@data = split(/ \|\|\| /, $line);
                @P = (); 
		@P = @data[0..1];
		$pair = ();
		$pair = "$P[0] ||| $P[1]";		
		if (!defined($Output{$pair})) {
			for ($Q=0; $Q<=1; $Q++) { @{ $Output{$pair}[$Q] } = (0,0); }
		}

		$log_counter++;
		print "$log_counter\n";

		@pos2word = ();
		wordDataStruct(\@P, \@pos2word);

                $n = scalar(@data);

		@O = (); 
		@O = split(/ /, $data[$n-1]);
		$p_G = ();
		$p_G = $O[0]/$O[1];	

                @struct = (); 
		@struct = @data[2..$n-2];

                $n = scalar(@struct);

                @labelStruct = (); 
		@labelStruct = @struct[$n/2..$n-1];
		expandGAPs(\@labelStruct);
	
		@L = ();
		@null_L = ();
		distinguishComps(\@labelStruct, \@L, \@null_L);

		if ( (scalar(@{$L[0]}) > $MaxNoComps) || (scalar(@{$L[1]}) > $MaxNoComps) ) { next; }

		%C2P = ();
		@Probs = ();
		for ($Q=0; $Q<=1; $Q++) { @{ $Probs[$Q] } = (0,0); }
	
		if ( (scalar(@{$L[0]}) == scalar(@labelStruct)) && (scalar(@{$L[1]}) == scalar(@labelStruct)) ) {
			$MaxI = 0;
		}
		else {
			$MaxI = 1;
		}
		
		computeProbabilities(\@L, \@null_L, $MaxI, \@pos2word, \%C2P, $_UC_, $_GC_, \@Probs);

		for ($Q=0; $Q<=1; $Q++) {
			for ($i=0; $i<=1; $i++) {
				$Output{$pair}[$Q][$i] += $p_G * $Probs[$Q][$i];
			}
		} 
	}
	close(TBP);

	system("rm  $nameDir/$nameDir.$M");

	printOutput(\%Output, $proc);

	return($log_counter);
}

#########################################################################################################################

sub computeProbabilities {

my	($K, $null_K, $MaxI, $pos2word, $C2P, $_UC_, $_GC_, $Probs) = @_;

my	($I, $x, $k, $c, $pair, $Q, $i, $unaligned);

my	@P;
my      @comps;
my      @clean_comps;
my	@labels;
my      @remove;
my      @PROD;
my	@Perm;
my	@L;        
my	@Z;

	for ($I=0; $I<=$MaxI; $I++) {

		@L = ();
		@L = @{ $$K[$I] };

		@Perm = ();
		@Perm = permutations(\@L);

	        for ($x=0; $x<scalar(@Perm); $x++) {

			@PROD = ();		

			if ($MaxI == 0) {
				@{ $PROD[0][0] } = (1,1);
				@{ $PROD[1][0] } = (1,1);
			}
			elsif ($MaxI == 1) {
				$PROD[0][$I] = 1;
				$PROD[1][$I] = 1;
			}

	                @remove = ();
        	        for ($k=0; $k<scalar(@L); $k++) {

                		@comps = ();
                        	getComplement(\@remove, \@L, \@comps);
				@clean_comps = ();
				@clean_comps = @comps;
				if (defined($$null_K[$I])) {
					$unaligned = 0;
					foreach $c (@clean_comps) {
						@labels = ();
						@labels = split(/ XXX /, $c);
						if ($labels[1-$I] eq 'NULL') { $unaligned++; }
					}
					if ($unaligned < scalar(@clean_comps)) {	 
						push(@comps, @{ $$null_K[$I] } );
					}
				}

	                        @P = ();
				getPhrasePair(\@P, $C2P, \@comps, $pos2word);
				$pair = ();
				$pair = "$P[0] ||| $P[1]";

				if ($pair !~ /GAP/) {
					trlProbability($_UC_, $pair, \@PROD, $MaxI, $I);
				}
				else {
					trlProbability($_GC_, $pair, \@PROD, $MaxI, $I);
				}
				if ($k == scalar(@L)-1) { next; }

				@Z = ();
				if    ($MaxI == 0) { @Z = (0,0); }
				elsif ($MaxI == 1) { $Z[$I] = 0; }
				$pair = ();
				$pair = sideNormalize(\@clean_comps, $C2P, $pos2word, \@Z, $_UC_, $_GC_, $Perm[$x][$k], $MaxI, $I);

				if ($pair !~ /GAP/) {
					sideProbability($_UC_, $pair, \@PROD, \@Z, \@clean_comps, $MaxI, $I);
				}
				else {
					sideProbability($_GC_, $pair, \@PROD, \@Z, \@clean_comps, $MaxI, $I);
				}

                        	push( @remove, $Perm[$x][$k] );
                	}

		        for ($Q=0; $Q<scalar(@$Probs); $Q++) {
				if ($MaxI == 0) {
					for ($i=0; $i<=1; $i++) {
		                		$$Probs[$Q][$i] += $PROD[$Q][0][$i];
					}
				}
				elsif ($MaxI == 1) {
					$$Probs[$Q][$I] += $PROD[$Q][$I];
				}
        		}
		}
	}
}

#########################################################################################################################

sub sideNormalize {

my	($comps, $C2P, $pos2word, $Z, $_UC_, $_GC_, $query, $MaxI, $I) = @_;

my	($c, $pair, $z);
my	@U;
my	@local;

	for ($c=0; $c<scalar(@$comps); $c++) {
        	@U = ();
                @local = ();
                @local = ($$comps[$c]);
                getPhrasePair(\@U, $C2P, \@local, $pos2word);
                $pair = ();
                $pair = "$U[0] ||| $U[1]";
		if ($pair !~ /GAP/) {
			if ($MaxI == 0) {
	                	for ($z=0; $z<=1; $z++) { $$Z[$z] += $$_UC_{$pair}[$z+1]; }
			}
			elsif ($MaxI == 1) {
				$$Z[$I] += $$_UC_{$pair}[$I+1];
			}
		}
		else {
			if ($MaxI == 0) {
	                        for ($z=0; $z<=1; $z++) { $$Z[$z] += $$_GC_{$pair}[$z+1]; }
                	}
			elsif ($MaxI == 1) {
				$$Z[$I] += $$_GC_{$pair}[$I+1];
			}
		}
	}
        @U = ();
        @U = @{ $$C2P{$query} };
        $pair = ();
        $pair = "$U[0] ||| $U[1]";
	return($pair);
}

#########################################################################################################################

sub trlProbability {

my	($_D_, $pair, $PROD, $MaxI, $I) = @_;

my	($sideCount, $condProb, $i, $Q);
my	$jointCount = ();

	$jointCount = $$_D_{$pair}[0];

	if ($MaxI == 0) {
	        for ($i=0; $i<=1; $i++) {
        		$sideCount  = ();
                	$sideCount  = $$_D_{$pair}[$i+1];
	                $condProb   = ();
        	        $condProb   = $jointCount / $sideCount;
                	for ($Q=0; $Q<scalar(@$PROD); $Q++) {
                		$$PROD[$Q][0][$i] *= $condProb;
                	}
		}
	}
	elsif ($MaxI == 1) {
		$sideCount  = ();
		$sideCount  = $$_D_{$pair}[$I+1];
		$condProb   = ();
		$condProb   = $jointCount / $sideCount;
		for ($Q=0; $Q<scalar(@$PROD); $Q++) {
			$$PROD[$Q][$I] *= $condProb;
		}
	}	
}

#########################################################################################################################

sub sideProbability {

my	($_D_, $pair, $PROD, $Z, $comps, $MaxI, $I) = @_;

my	($i, $sideCount, $sideProb);

	if ($MaxI == 0) {
		for ($i=0; $i<=1; $i++) {
			$sideCount  = ();
			$sideCount  = $$_D_{$pair}[$i+1];
			$sideProb   = ();
			$sideProb   = $sideCount / $$Z[$i];

			$$PROD[0][0][$i] *= $sideProb;
			$$PROD[1][0][$i] *= 1 / scalar(@$comps);
		}
	}
	elsif ($MaxI == 1) {
		$sideCount  = ();
		$sideCount  = $$_D_{$pair}[$I+1];
		$sideProb   = ();
		$sideProb   = $sideCount / $$Z[$I];

		$$PROD[0][$I] *= $sideProb;
		$$PROD[1][$I] *= 1 / scalar(@$comps);
	}
}

#########################################################################################################################

sub getPhrasePair {

my	($P, $C2P, $comps, $pos2word) = @_;

my	$hash_comps = ();
	
	$hash_comps = join(' ||| ', @$comps);
        
        if (defined($$C2P{$hash_comps})) {
	        @$P = @{ $$C2P{$hash_comps} };
        }
        else {
        	formPhrases($comps, $pos2word, $P);
                @{ $$C2P{$hash_comps} } = @$P;
        }
}

#########################################################################################################################

sub formPhrases {

my      ($L, $pos2word, $P) = @_;

my      ($n, $k, $i, $l, $j);
my      @comp;
my      @loc_labels;

my      @labels = ();
my      @sorted_labels = ();
my      @list_pair = ();

        $n = scalar(@$L);

        for ($k=0; $k<$n; $k++) {
                @comp = ();
                @comp = split(/ XXX /, $$L[$k]);
                for ($i=0; $i<=1; $i++) {
                        $comp[$i] =~ s/GAP //g;
                        @loc_labels = ();
                        @loc_labels = split(/ /, $comp[$i]);
                        for ($l=0; $l<scalar(@loc_labels); $l++) {
                                if ($loc_labels[$l] eq 'NULL') {
                                        $loc_labels[$l] = ();
                                        $loc_labels[$l] = 99;
                                }
                        }
                        push( @{ $labels[$i] }, @loc_labels);
                }
        }

	labels2pair(\@labels, $pos2word, $P);
}

#########################################################################################################################

sub findPairCounts {

my	($_D_, $countFile) = @_;

my	$u;
my      @P;
my      @c;

        open(FIN, "<$countFile");
        while(<FIN>) {
                chomp;
                @P = ();
                @P = split(/ \|\|\| /);
                $u = "$P[0] ||| $P[1]";
                if (!defined($$_D_{$u})) { next; }
                @c = ();
                @c = split(/ /, $P[2]);
                @{ $$_D_{$u} } = @c;
        }
        close(FIN);
}

#########################################################################################################################

sub storePairsInMemory {

my	($_UC_, $_GC_, $nameDir, $M) = @_;

my	($n, $line);
my	@data;
my	@struct;
my	@P;
my	@L;
my	@pos2word;

	open(TBP, "<$nameDir/$nameDir.$M");
	while($line=<TBP>) {

		chomp($line);
		@data = ();
		@data = split(/ \|\|\| /, $line);
		@P = ();
		@P = @data[0..1];
		$n = scalar(@data);
		@struct = ();
		@struct = @data[2..$n-2];
		$n = scalar(@struct); 
		@L = ();
        	@L = @struct[$n/2..$n-1];
		@pos2word = ();

		wordDataStruct(\@P, \@pos2word);
		allSubPairs(\@L, \@pos2word, $_UC_, $_GC_); 
	}
	close(TBP);
}

#########################################################################################################################

sub allSubPairs {

my	($L, $pos2word, $_UC_, $_GC_) = @_;

my	($n, $j, $full_string, $string, $k, $bit, $i, $l, $NoC, $flag, $find);

my	@comp;
my	@loc_labels;
my	@labels;
my	@CheckBothNULL;
my	@pair;

        $n = scalar(@$L);
        for ($j=1; $j<2**$n; $j++) {
                $full_string = dec2bin($j);
                $string = substr($full_string, -$n, $n );
		@labels = ();
		$NoC = 0;
		@CheckBothNULL = ();
		$flag = 1;
                for ($k=0; $k<$n; $k++) {
                        $bit = substr($string, $k, 1);
			$NoC += $bit;
                        if ($bit eq "0") { next; }
			push( @CheckBothNULL, $$L[$k] );
			@comp = ();
			@comp = split(/ XXX /, $$L[$k]);
			for ($i=0; $i<=1; $i++) {
				$comp[$i] =~ s/GAP //g;
				@loc_labels = ();
				@loc_labels = split(/ /, $comp[$i]);
				for ($l=0; $l<scalar(@loc_labels); $l++) {
					if ($loc_labels[$l] eq 'NULL') {
						$loc_labels[$l] = ();
						$loc_labels[$l] = 99;
					}
				}
				push( @{ $labels[$i] }, @loc_labels);
			}		
                }

		if ($NoC > 1) { $flag = checkUnaligned(\@CheckBothNULL); }
		if ($flag == 0) { next; }

		@pair = ();
		labels2pair(\@labels, $pos2word, \@pair);
		$find = join(' ||| ', @pair);
	        #if ($NoC == 1) { @{ $$_CC_{$find} } = (-1,-1,-1); }
        	#else           { @{ $$_UC_{$find} } = (-1,-1,-1); }
        	if ($find !~ /GAP/) {
	                @{ $$_UC_{$find} } = (-1,-1,-1);
		}
		else {
			@{ $$_GC_{$find} } = (-1,-1,-1);
		}
        }
}

#########################################################################################################################

sub labels2pair {

my	($labels, $pos2word, $pair) = @_;

my	($i, $j, $w, $diff, $g);
my	@sorted_labels = ();
my	@list_pair = ();
my	@dummy;

	for ($i=0; $i<=1; $i++) {
		@{ $sorted_labels[$i] } = sort { $a <=> $b } @{ $$labels[$i] };
		$w = $$pos2word[$i]{$sorted_labels[$i][0]};
		push( @{ $list_pair[$i] }, $w);
		if ($w eq 'NULL') { next; }
		for ($j=1; $j<scalar(@{ $sorted_labels[$i] }); $j++) {
			$w = $$pos2word[$i]{$sorted_labels[$i][$j]};
			if ($w eq 'NULL') { last; }
			$diff = $sorted_labels[$i][$j] - $sorted_labels[$i][$j-1];

                        if ($diff != 1) {
                                @dummy = ();
                                for ($g=1; $g<$diff; $g++) {
                                        push(@dummy , 'GAP');
                                }
                                push(@dummy, $w);
                                $w = ();
                                $w = join(' ', @dummy);
                        }

			push( @{ $list_pair[$i] }, $w);
		}
	}

	for ($i=0; $i<=1; $i++) { $$pair[$i] = join(' ', @{ $list_pair[$i] }); }
}

#########################################################################################################################

sub wordDataStruct {

my      ($P, $pos2word) = @_;

my      ($i, $j);
my      @tokens;

        for ($i=0; $i<=1; $i++) {
                @tokens = ();
                @tokens = split(/ /, $$P[$i]);
                for ($j=0; $j<scalar(@tokens); $j++) {
                        $$pos2word[$i]{$j} = $tokens[$j];
                }
                $$pos2word[$i]{99} = 'NULL';
        }
}

#########################################################################################################################

sub getComplement {

my      ($Some, $Full, $_Some) = @_;
my      $compare = ();
my      @_Some = ();

        $compare = List::Compare->new('-a', \@$Some, \@$Full);
        @$_Some = $compare->get_complement;
}

#########################################################################################################################

sub checkUnaligned {

my      ($CheckBothNULL) = @_;

my      ($i, $j, $k);
my      @comp;
my      $flag = 0;
my      @sideNull = (0,0);

        for ($k=0; $k<scalar(@$CheckBothNULL); $k++) {
                @comp = ();
                @comp = split(/ XXX /, $$CheckBothNULL[$k]);
                for ($i=0; $i<=1; $i++) {
                        if ($comp[$i] eq 'NULL') {
                                $flag++;
                                $sideNull[$i] = 1;
                        }
                }
        }

        if ($flag == scalar(@$CheckBothNULL)) {
                if ($sideNull[0] + $sideNull[1] == 1) { return(1); }
                else                                  { return(0); }
        }
        else {
                return(1);
        }
}

#########################################################################################################################

sub dec2bin { return unpack("B32", pack("N", shift)); }

#########################################################################################################################

sub distinguishComps {

my	($labelStruct, $L, $null_L) = @_;

my	($k,$i);
my	@c;

	for ($k=0; $k<scalar(@$labelStruct); $k++) {
		@c = ();
		@c = split(/ XXX /, $$labelStruct[$k]);
		for ($i=0; $i<=1; $i++) {
			if ($c[$i] eq 'NULL') { 
				push( @{ $$null_L[$i] }, $$labelStruct[$k] );
			}
			else {
				push( @{ $$L[$i] }, $$labelStruct[$k] );
			}
		}
	}
}

#########################################################################################################################

sub fewerDigits {

my      ($x, $NoD)=@_;
my      ($integer, $fractional, $less, $mantissa, $exponent);

        if ($x !~ 'e') {
                ($integer, $fractional) = split(/\./, $x);
                if (defined($fractional)) {
                        if (length($fractional) > $NoD) {
                                $less = substr($fractional, 0, $NoD);
                                $x = "$integer.$less";
                        }
                }
        }
        else {
                ($mantissa, $exponent) = split(/e-/, $x);
                ($integer, $fractional) = split(/\./, $mantissa);
                if (defined($fractional)) {
                        if (length($fractional) > $NoD) {
                                $less = substr($fractional, 0, $NoD);
                                $x = "$integer.$less".'e-'.$exponent;
                        }
                }
        }
        return($x);
}

#########################################################################################################################

sub printOutput {

my	($Output, $proc) = @_;

my	($Q, $pair, $i);
my	@prob;

        for ($Q=0; $Q<=1; $Q++) {
                open(PRB, ">>exactProbabilities.$Q.$proc");
                foreach $pair (keys(%$Output)) {
                        @prob = ();
                        for ($i=0; $i<=1; $i++) {
                                $prob[$i] = fewerDigits($$Output{$pair}[$Q][$i], 8);
                        }
                        print PRB "$pair ||| $prob[1] $prob[0]\n";
                }
                close(PRB);
        }
}

#########################################################################################################################

sub expandGAPs {

my	($labelStruct) = @_;

my	($c, $i, $j, $diff, $g, $l, $e_c);
my	@comp;
my	@l_c;
my	@labels;
my	@list;
my	@dummy;
my	@store = ();

	foreach $c (@$labelStruct) {
		$c =~ s/GAP //g;
		@comp = ();
		@comp = split(/ XXX /, $c);
		@l_c = ();
		for ($i=0; $i<=1; $i++) {
			@list   = ();
			@labels = ();
			@labels = split(/ /, $comp[$i]);
			push( @list, $labels[0] );
			for ($j=1; $j<scalar(@labels); $j++) {
				$l = $labels[$j];
				$diff = $labels[$j] - $labels[$j-1];
				if ($diff != 1) {
					@dummy = ();
					for ($g=1; $g<$diff; $g++) {
						push(@dummy , 'GAP');
					}
					push(@dummy, $l);
					$l = ();
					$l = join(' ', @dummy);
				}
				push( @list, $l);
			}
			$l_c[$i] = join(' ', @list); 
		}
		$e_c = ();
		$e_c = join(' XXX ', @l_c);
		push( @store, $e_c);
	}
	
	@$labelStruct = ();
	@$labelStruct = @store;
}

#########################################################################################################################

