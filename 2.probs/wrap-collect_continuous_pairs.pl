#!/usr/bin/perl -w

($ID, $U, $G, $packets, $nops, $NoP)=@ARGV;

if(@ARGV!=6) {
    print STDERR "\nUSAGE ::: <id.phrase.0 (DIR)>   <phrasepairs.cont.0 (DIR)>   <phrasepairs.disc.0 (DIR)>  <# packets>  <# procs>  <n pairs for P(n)>\n";
    exit(-1);
}
use POSIX;
###########################################################################################################################################################

@handler=();
$packet_size = floor( $nops / 2 );
for ($k=0; $k<2; $k++ ) {
        $handler[$k][0] = $k*$packet_size+1;
        $handler[$k][1] = ($k+1)*$packet_size;
}
$handler[0][0] = 0;
$handler[1][1] = $nops;


for ($k=0; $k<2; $k++) {
        for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
                system("nohup ./compute-remove_IDs.pl  $job  $ID  2>>err.$job  &");
        }
        flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
}

system("mkdir  temp.cont.phrase.0");
system("mv     temp.cont.phrase.0.*   temp.cont.phrase.0");


for ($k=0; $k<2; $k++) {
	for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
		system("nohup ./compute-local_pair_component_counts.pl  $job  temp.cont.phrase.0  2>>err.$job  &");
	}
	flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
}
###########################################################################################################################################################

@handler=();
$packet_size = floor( $nops / $packets );
for ($k=0; $k<$packets; $k++ ) {
        $handler[$k][0] = $k*$packet_size+1;
        $handler[$k][1] = ($k+1)*$packet_size;
}
$handler[0][0] = 0;
$handler[$packets-1][1] = $nops;


for ($k=0; $k<$packets; $k++) {
       	for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
               	system("nohup ./compute-pair_component_counts.pl  $job  temp.cont.phrase.0  $nops  2>>err.$job  &");
       	}
       	flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
}

system("rm  -r  temp.cont.phrase.0");
system("mkdir  continuous.phrase.0");	
system("mv     continuous.phrase.0.*   continuous.phrase.0");

###########################################################################################################################################################

#for ($k=0; $k<$packets; $k++) {
#        for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
#                system("nohup ./compute-insert_missing_cocos.pl  $job  $C  missingCocos.$job  $nops  &");
#        }
#        flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
#}

###########################################################################################################################################################

#for ($k=0; $k<$packets; $k++) {
#        for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
#                system("nohup ./compute-update_component_list.pl  $job  $C  missingCocos.$job  &");
#        }
#        flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
#}

###########################################################################################################################################################

for ($k=0; $k<$packets; $k++) {
        for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
                system("nohup ./compute-insert_missing_unions.pl  $job  $U  $G  missingUnions.$job  $NoP  $nops  2>>err.$job  &");
        }
        flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
}

###########################################################################################################################################################

$packets = 3;
@handler=();
$packet_size = floor( $nops / $packets );
for ($k=0; $k<$packets; $k++ ) {
        $handler[$k][0] = $k*$packet_size+1;
        $handler[$k][1] = ($k+1)*$packet_size;
}
$handler[0][0] = 0;
$handler[$packets-1][1] = $nops;

for ($k=0; $k<$packets; $k++) {
        for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
                system("nohup ./compute-update_union_list.pl   $job  $U  updated.missing.cont.$job  2>>err.$job  &");
        }
        flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
}

for ($k=0; $k<$packets; $k++) {
        for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
                system("nohup ./compute-update_union_list.pl   $job  $G  updated.missing.disc.$job  2>>err.$job  &");
        }
        flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
}

###########################################################################################################################################################

system("touch  DONE.UPDATE");

###########################################################################################################################################################

sub flagging {

my      ($start, $finish, $name)=@_;
my      $job;
my      @flag_job=();
my      $flags=0;
my      $NoJ;

        $NoJ = $finish - $start + 1;

        for ($job=$start; $job <= $finish; $job++) { $flag_job[$job]=0; }

        while($flags != $NoJ) {
                for ($job=$start; $job <= $finish; $job++) {
                        if (-e "$name.$job") { $flag_job[$job]=1;}
                }
                $flags=0;
                for ($job=$start; $job <= $finish; $job++) {
                        $flags += $flag_job[$job];
                }
        }
        for ($job=$start; $job <= $finish; $job++) { qx(rm $name.$job);  }
        return();
}


