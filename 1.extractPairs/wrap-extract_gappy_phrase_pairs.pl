#!/usr/bin/perl -w

($I, $batch, $packets, $nops)=@ARGV;

if(@ARGV!=4) {
    print STDERR "\nUSAGE ::: <id.phrase.batch (DIR)>  <batch #>   <# packets>  <# procs>\n";
    exit(-1);
}
use POSIX;

###########################################################################################################################################################

$packet_size = floor( $nops / $packets );
@handler=();
for ($k=0; $k<$packets; $k++ ) {
        $handler[$k][0] = $k*$packet_size+1;
        $handler[$k][1] = ($k+1)*$packet_size;
}
$handler[0][0]=0;
$handler[$packets-1][1] = $nops;

for ($k=0; $k<$packets; $k++) {
        for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
		$file = "$I/*.$job";
		system("nohup ./compute-extract_gappy_phrase_pairs.pl  $job  $batch  $file   1>>log.$batch.$job  2>>err.$batch.$job  &");
	}
	flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
}

system("mkdir  phrasepairs.cont.$batch");
system("mv     phrasepairs.cont.$batch.*   phrasepairs.cont.$batch");
system("mkdir  phrasepairs.disc.$batch");
system("mv     phrasepairs.disc.$batch.*   phrasepairs.disc.$batch");
system("mkdir  x.id.phrase.$batch");
system("mv     x.id.phrase.$batch.*  x.id.phrase.$batch");

system("touch  DONE.UGAP");

###########################################################################################################################################################

sub flagging {

my      ($start, $finish, $name)=@_;
my      $job;
my      @flag_job=();
my      $flags=0;
my      $NoJ;

        $NoJ = $finish - $start + 1;

        for ($job=$start; $job <= $finish; $job++) { $flag_job[$job]=0; }

        while($flags != $NoJ) {
                for ($job=$start; $job <= $finish; $job++) {
                        if (-e "$name.$job") { $flag_job[$job]=1;}
                }
                $flags=0;
                for ($job=$start; $job <= $finish; $job++) {
                        $flags += $flag_job[$job];
                }
                sleep(1);
        }
        for ($job=$start; $job <= $finish; $job++) { qx(rm $name.$job);  }
        return();
}

###########################################################################################################################################################
=pod
sub flagging {

my      ($nops, $name, $offset)=@_;
my      $job;
my      @flag_job;
my      $flags=0;

        for ($job=$offset; $job <= $nops; $job++) { $flag_job[$job]=0; }

        while($flags != ($nops-$offset+1)) {
                for ($job=$offset; $job <= $nops; $job++) {
                        if (-e "$name.$job") { $flag_job[$job]=1;}
                }
                $flags=0;
                for ($job=$offset; $job <= $nops; $job++) {
                        $flags += $flag_job[$job];
                }
        }
        for ($job=$offset; $job <= $nops; $job++) { qx(rm $name.$job);  }
        return();
}
=cut
