#!/usr/bin/perl -w

($B, $A, $L, $batch, $packets, $nops, $NBL)=@ARGV;

if(@ARGV!=7) {
    print STDERR "\nUSAGE ::: <BiLing Segmentation List (DIR)>  <Alignment Corpus (DIR)>  <Lexical Weights (DIR)>  <batch #>   <# packets>  <# procs>   <length of N-best list>\n";
    exit(-1);
}
use POSIX;

############################################################################################################################################
############################################################################################################################################

system("nohup ./wrap-empirical.pl  $B  $A  $L  $batch  $packets  $nops  $NBL  &");

while(!(-e 'DONE.EMP')) { sleep(1); }
system("rm  DONE.EMP");

############################################################################################################################################

system("nohup ./wrap-extract_gappy_phrase_pairs.pl  id.phrase.$batch   $batch   $packets   $nops  &");

while(!(-e 'DONE.UGAP')) { sleep(1); }
system("rm  DONE.UGAP");

############################################################################################################################################

#system("nohup ./wrap-cat_unions_with_gappy.pl  unions.phrase.$batch   gappy.phrase.$batch   $batch   $packets   $nops  &");

#while(!(-e 'DONE.ALLUNI')) { sleep(1); }
#system("rm  DONE.ALLUNI");

system("rm  -r  unions.phrase.$batch  comps.phrase.$batch"); 

############################################################################################################################################

