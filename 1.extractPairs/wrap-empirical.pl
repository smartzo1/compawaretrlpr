#!/usr/bin/perl -w

($B, $A, $L, $batch, $packets, $nops, $NBL)=@ARGV;

if(@ARGV!=7) {
    print STDERR "\nUSAGE ::: <BiLing Segmentation List (DIR)>  <Alignment Corpus (DIR)>  <Lexical Weights (DIR)>  <batch #>   <# packets>  <# procs>   <length of N-best list>\n";
    exit(-1);
}
use POSIX;

###########################################################################################################################################################

$packet_size = floor( $nops / $packets );
@handler=();
for ($k=0; $k<$packets; $k++ ) {
        $handler[$k][0] = $k*$packet_size+1;
        $handler[$k][1] = ($k+1)*$packet_size;
}
$handler[0][0]=0;
$handler[$packets-1][1] = $nops;

for ($k=0; $k<$packets; $k++) {
        for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
		$biLingSegFile = "$B/*.$batch.$job";
		$alignmentFile = "$A/*.$batch.$job";
		$lexWeightFile = "$L/*.$batch.$job";
		system("nohup ./compute-empirical.pl  $batch  $job  $biLingSegFile  $alignmentFile  $lexWeightFile  $NBL  1>log.$batch.$job  2>err.$batch.$job  &");
	}
	flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
}
organize('words');
organize('segms');
organize('other');

system("touch  DONE.EMP");

###########################################################################################################################################################

sub organize {

my      ($T) = @_;

my      @X = ('phrase', 'align', 'orient');
#my      @M = ('cc', 'cssg', 'occurr');
my      @M = ('cnt');
my	@o = (); 
my      ($i, $j, $name);

        if ($T =~ /segms/) {
        	system("mkdir $T.$batch");
                for ($j=0; $j<scalar(@M); $j++) {
                        system("mkdir  $T.$batch/$M[$j].$batch");
                }
                for ($i=1; $i<scalar(@X); $i++) {
                        for ($j=0; $j<scalar(@M); $j++) {
                                $name = "$T.$X[$i].$M[$j].$batch";
                                system("mkdir  $name");
                                system("mv     $name.*  $name");
                                system("mv     $name    $T.$batch/$M[$j].$batch");
                        }
                }
        }
        elsif ($T eq 'words') {
        	system("mkdir $T.$batch");
                for ($i=0; $i<scalar(@X); $i++) {
                        $name = "$T.$X[$i].$batch";
                        system("mkdir $name");
                        system("mv  $name.*  $name");
                        system("mv  $name    $T.$batch");
                }
        }
	elsif ($T eq 'other') {
		@o = ('comps.phrase', 'id.phrase', 'unions.phrase');
		for ($i=0; $i<scalar(@o); $i++) {
			system("mkdir  $o[$i].$batch");
			system("mv  $o[$i].$batch.*  $o[$i].$batch");
		}
	}

}

###########################################################################################################################################################

sub flagging {

my      ($start, $finish, $name)=@_;
my      $job;
my      @flag_job=();
my      $flags=0;
my      $NoJ;

        $NoJ = $finish - $start + 1;

        for ($job=$start; $job <= $finish; $job++) { $flag_job[$job]=0; }

        while($flags != $NoJ) {
                for ($job=$start; $job <= $finish; $job++) {
                        if (-e "$name.$job") { $flag_job[$job]=1;}
                }
                $flags=0;
                for ($job=$start; $job <= $finish; $job++) {
                        $flags += $flag_job[$job];
                }
                sleep(1);
        }
        for ($job=$start; $job <= $finish; $job++) { qx(rm $name.$job);  }
        return();
}

###########################################################################################################################################################
=pod
sub flagging {

my      ($nops, $name, $offset)=@_;
my      $job;
my      @flag_job;
my      $flags=0;

        for ($job=$offset; $job <= $nops; $job++) { $flag_job[$job]=0; }

        while($flags != ($nops-$offset+1)) {
                for ($job=$offset; $job <= $nops; $job++) {
                        if (-e "$name.$job") { $flag_job[$job]=1;}
                }
                $flags=0;
                for ($job=$offset; $job <= $nops; $job++) {
                        $flags += $flag_job[$job];
                }
        }
        for ($job=$offset; $job <= $nops; $job++) { qx(rm $name.$job);  }
        return();
}
=cut
