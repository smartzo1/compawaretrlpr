#!/usr/bin/perl -w

use strict;

my ($batch, $proc, $segmentationsFile, $alignmentFile, $lexProbFile, $NBL)=@ARGV;

if(@ARGV!=6) {
    print STDERR "\nUSAGE ::: <batch #>   <proc #>   <segmentations>   <alignments>   <lexical probabilities>   <length of N-best list>\n";
    exit(-1);
}
BEGIN { unshift @INC, qw( /home/smartzo1/Perl_spyros/Graph-0.96/lib/ ); }
#BEGIN { unshift @INC, qw( /home/spyros/Perl_spyros/Graph-0.94/lib/ ); }

BEGIN { unshift @INC, qw( /home/smartzo1/Perl_spyros/ExtUtils-PkgConfig-1.15/lib); }
#BEGIN { unshift @INC, qw( /home/spyros/Perl_spyros/ExtUtils-PkgConfig-1.12/lib); }

BEGIN { unshift @INC, qw( /home/smartzo1/Perl_spyros/List-Compare-0.38/lib/ ); }
#BEGIN { unshift @INC, qw( /home/spyros/Perl_spyros/List-Compare-0.37/lib/ ); }

BEGIN { unshift @INC, qw( /home/smartzo1/Perl_spyros/Algorithm-Combinatorics-0.27/blib/arch/); }
#BEGIN { unshift @INC, qw( /home/spyros/Perl_spyros/Algorithm-Combinatorics-0.27/blib/arch/); }

BEGIN { unshift @INC, qw( /home/smartzo1/Perl_spyros/Algorithm-Combinatorics-0.27/blib/lib); }
#BEGIN { unshift @INC, qw( /home/spyros/Perl_spyros/Algorithm-Combinatorics-0.27/blib/lib); }

use Algorithm::Combinatorics qw(subsets permutations);
use List::Compare;
use Graph::Undirected;
use List::Util qw(first max maxstr min minstr reduce shuffle sum);
use POSIX;
#############################################################################################################################################################

my $___MaxPhraseLength___ = 7;
my $___MaxNoSubGraphSamples___ = 100;

#############################################################################################################################################################

my $SEG;
my $ALG;
my $LEX;

open($SEG, "<$segmentationsFile");
open($ALG, "<$alignmentFile");
open($LEX, "<$lexProbFile");
my $___WFH___ = openOutputFiles('words');
my $___SFH___ = openOutputFiles('segms');

my @line = ('VOID', 'VOID', 'VOID');
my @read = (0,0,0);
my $firstLine = 1;
my $COUNTER = 1;

my @sentence;
my %BiSegmentation;
my @LexProbs;
my $alignment;

while ($line[0] and $line[1] and $line[2]) {
        if ( ($read[0] == 0) && ($read[1] == 0) && ($read[2] == 0) ) {
                if ($firstLine == 0) {
                        print "{$COUNTER}\n";
                        work(\@sentence, \%BiSegmentation, $alignment, \@LexProbs);
                        $COUNTER++;
#if ($COUNTER == 100) { last; }
                }
                else {
                        $firstLine = 0;
			$line[0] = read_line($SEG);
			$line[2] = read_line($LEX);
                }
                @sentence = ();
	        %BiSegmentation = ();
		@LexProbs = ();
                $alignment = ();
                $line[0] =~ s/\t//;
                if ($line[0] eq 'VOID') { last; }
                @sentence = split(/ XXX /, $line[0]);
                @read = (1,1,1);
        }
        elsif ( ($read[0] == 1) && ($read[1] == 1) && ($read[2] == 1) ) {
	        @line = (read_line($SEG), read_line($ALG), read_line($LEX));
                storeInput($line[0], \%BiSegmentation, $NBL);
                $alignment = $line[1];
		storeInput($line[2], \@LexProbs, -1);
                @read = (1,0,1);
        }
        elsif ( ($read[0] == 1) && ($read[1] == 0) && ($read[2] == 1) ) {
                $line[0] = read_line($SEG);
		$line[2] = read_line($LEX);
                if (!defined($line[0])) { $line[0] = "\tVOID"; }
		if (!defined($line[2])) { $line[2] = "\tVOID"; }
		#$read[1] = 0;

                if ($line[0] =~ /^\t/) {
                	$read[0] = 0;
                }
                else {
                        storeInput($line[0], \%BiSegmentation, $NBL);
                        $read[0] = 1;
                }

                if ($line[2] =~ /^\t/) {
                        $read[2] = 0;
                }
                else {
                        storeInput($line[2], \@LexProbs, -1);
                        $read[2] = 1;
                }
        }
	elsif ( ($read[0] == 1) && ($read[1] == 0) && ($read[2] == 0) ) {
		$line[0] = read_line($SEG);
		if (!defined($line[0])) { $line[0] = "\tVOID"; }
                if ($line[0] =~ /^\t/) {
                        $read[0] = 0;
                }
                else {
                        storeInput($line[0], \%BiSegmentation, $NBL);
                        $read[0] = 1;
                }
	}
        elsif ( ($read[0] == 0) && ($read[1] == 0) && ($read[2] == 1) ) {
                $line[2] = read_line($LEX);
                if (!defined($line[2])) { $line[2] = "\tVOID"; }
                if ($line[2] =~ /^\t/) {
                        $read[2] = 0;
                }
                else {
                        storeInput($line[2], \@LexProbs, -1);
                        $read[2] = 1;
                }
        }
}
close($SEG);
close($ALG);
close($LEX);
closeOutputFiles('words', $___WFH___);
closeOutputFiles('segms', $___SFH___);

##########################################################################################################################################################

system("touch _WORK_.$proc");

##########################################################################################################################################################

sub work {

my      ($sentence, $BiSegmentation, $alignment, $LexProbs) = @_;

my      @pos2word = ();
my      @word2pos = ();
my      @A = ();
my	%invA = ();
my      @srcWordPos2trgWordPos = ();
my      %srcWord2trgWord = ();
my      %_TrlSet_ = ();
my	@unSegmented = ();
my	$flag = 0;
my	@sentLength = ();

	$flag = basicDataStructures($sentence, $alignment, $BiSegmentation, \@pos2word, \@word2pos, \@A, \%invA, \@srcWordPos2trgWordPos, \%srcWord2trgWord, \%_TrlSet_, $flag, \@unSegmented, \@sentLength);

my	($biString, $i);
my	@string;

my	@pos2seg;
my	@seg2pos;
my	@word2segPos;
my	@length;

my  	@segA;
my 	@trg_segA;
my      @_SegSet_ = ();

my     	%OchPairs;
my      %PhrasePairHash = ();
my	%CompHash = ();
my	%Pair2Hash;

my	%Measure = ();
my	%Weight = ();
my	@NormWeight = ();
my	%Align = ();
my	%Orient = ();

my	%CompPairMeasure = ();

	foreach $biString (keys(%$BiSegmentation)) {

		@string = ();
		@string = split(/ XXX /, $biString);
		
		for ($i=0; $i<=1; $i++) {

			segmentDataStructures($i, $string[$i], \@pos2word, \@pos2seg, \@seg2pos, \@word2segPos, \@length);

		}

		segmentAlignment(\@pos2seg, \@srcWordPos2trgWordPos, \@segA, \@trg_segA);

		consistentPairs(\@length, \@segA, \@trg_segA, \@pos2seg, \%OchPairs);

		componentsOfConsistentPairs(\%OchPairs, \%_TrlSet_, \@_SegSet_, \%PhrasePairHash, \%CompHash, \%Pair2Hash, \%srcWord2trgWord, \@word2segPos, \@pos2seg, $LexProbs);

		collectStatistics($biString, \%OchPairs, \%PhrasePairHash, \%Pair2Hash, \%Measure, \%Weight, \@NormWeight, \@sentLength, \%srcWord2trgWord, \%Align, \%Orient, \@word2pos, \%invA, $flag, \@unSegmented, \%CompHash, \%CompPairMeasure);

	}

	expectedValues($BiSegmentation, \%Measure, \%Weight, \@NormWeight, \%Align, \%Orient, $flag, \@unSegmented, \%CompPairMeasure);
}

##########################################################################################################################################################

sub expectedValues {

my	($BiSegmentation, $Measure, $Weight, $NormWeight, $Align, $Orient, $flag, $unSegmented, $CompPairMeasure) = @_;

my	($pair, $biString, $m, $z, $H, $allow, $baseline);
my	@string;
my	%GenConsPhrase = ();
my	%GenConsAlign = ();
my	%GenConsOrient = ();
my	%GenConsComp = ();

	foreach $biString (keys(%$BiSegmentation)) {
		@string = ();
		@string = split(/ XXX /, $biString);

		($baseline, $allow) = (0, 0);
	        if ( ($$unSegmented[0] eq $string[0]) && ($$unSegmented[1] eq $string[1]) ) {
			$baseline = 1;
        		if ($flag == 0) { $allow = 1; }
			else 	        { $allow = 0; }
		}
		else { $allow = 1; }

                if ( ($baseline == 1) && ($allow == 1) ) { foreach $pair (keys(%{ $$Measure{$biString} })) {
                        printStandard($pair, $biString, $Measure, $Align, $Orient);
                        genConsStat($pair, $biString, $Measure, $Align, $Orient, $Weight, $NormWeight, \%GenConsPhrase, \%GenConsAlign, \%GenConsOrient);
                }}
                elsif ( ($baseline == 1) && ($allow == 0) ) { foreach $pair (keys(%{ $$Measure{$biString} })) {
                        printStandard($pair, $biString, $Measure, $Align, $Orient);
                }}
                elsif ( ($baseline == 0) && ($allow == 1) ) { foreach $pair (keys(%{ $$Measure{$biString} })) {
                        genConsStat($pair, $biString, $Measure, $Align, $Orient, $Weight, $NormWeight, \%GenConsPhrase, \%GenConsAlign, \%GenConsOrient);
                }}

		foreach $pair (keys(%{ $$CompPairMeasure{$biString} })) {
			for ($m=0; $m<scalar(@{ $$CompPairMeasure{$biString}{$pair} }); $m++) {
				$GenConsComp{$pair}[$m][0] = $$CompPairMeasure{$biString}{$pair}[$m];
			}
		}
	}		
	
	printGenConsStat(0, \%GenConsPhrase, \%GenConsAlign, \%GenConsOrient, \%GenConsComp);	
}

##########################################################################################################################################################

sub printStandard {

my	($pair, $Q, $M, $A, $O) = @_;

my	($m, $clean_pair, $H);
my	@output = ();

	$clean_pair = $pair;
        $clean_pair =~ s/\#\d+//g;

        #for ($m=0; $m<scalar(@{ $$M{$Q}{$pair} }); $m++) {
        #	$output[$m] = fewerDigits($$M{$Q}{$pair}[$m], 6);
        #}

        # @output = (1) x scalar(@{ $$M{$Q}{$pair} }); error!! it should be length of Normalizer
        @output = (1);

        $H = $$___WFH___[0];
        print $H "$clean_pair ||| @output\n";

        $H = $$___WFH___[1];
        print $H "$clean_pair ||| @{ $$A{$Q}{$pair} } ||| @output\n";

        $H = $$___WFH___[2];
        print $H "$clean_pair ||| @{ $$O{$Q}{$pair} } ||| @output\n";
}

##########################################################################################################################################################

sub printGenConsStat {

my	($I, $GP, $GA, $GO, $GC) = @_;

my	($pair, $NoM, $NoZ, $clean_pair, $m, $z, $H, $ali, $ori);
my	@output;
my	@components;

        foreach $pair (keys(%$GP)) {
	
		$NoM = scalar( @{ $$GP{$pair} } );
		$NoZ = scalar( @{ $$GP{$pair}[0] } );

                $clean_pair = $pair;
                $clean_pair =~ s/\#\d+//g;

		for ($m=0; $m<$NoM; $m++) {
			#
			print IDP "$COUNTER ||| $pair ||| $$GP{$pair}[$m][0]\n";
			#
			#@output = ();
			#for ($z=0; $z<$NoZ; $z++) { $output[$z] = fewerDigits($$GP{$pair}[$m][$z], 6); }
			#$H = $$___SFH___[$I][0][$m];
			#print $H "$clean_pair ||| @output\n";
			#$$GP{$pair}[$m][0] =~ s/\#\d+//g;
			#print $H "$clean_pair ||| $$GP{$pair}[$m][0] ||| 1\n";
			@components = ();
			@components = split(/ \|\|\| /, $$GP{$pair}[$m][0]);
			if (scalar(@components) == 1) { next; }
			print UNI "$clean_pair ||| 1\n";
		}

		foreach $ali (keys(%{$$GA{$pair}})) {
			for ($m=0; $m<$NoM; $m++) {
				@output = ();
				for ($z=0; $z<$NoZ; $z++) { $output[$z] = fewerDigits($$GA{$pair}{$ali}[$m][$z], 6); }
	                        $H = $$___SFH___[$I][1][$m];
        	                print $H "$clean_pair ||| $ali ||| @output\n";
			}
		}

		foreach $ori (keys(%{$$GO{$pair}})) {
			for ($m=0; $m<$NoM; $m++) {
				@output = ();
				for ($z=0; $z<$NoZ; $z++) { $output[$z] = fewerDigits($$GO{$pair}{$ori}[$m][$z], 6); }
                        	$H = $$___SFH___[$I][2][$m];
                        	print $H "$clean_pair ||| $ori ||| @output\n";
			}
		}
	}

	foreach $pair (keys(%$GC)) {

		$NoM = scalar( @{ $$GC{$pair} } );
		$NoZ = scalar( @{ $$GC{$pair}[0] } );

                $clean_pair = $pair;
                $clean_pair =~ s/\#\d+//g;

		for ($m=0; $m<$NoM; $m++) {
			@output = ();
			for ($z=0; $z<$NoZ; $z++) { $output[$z] = fewerDigits($$GC{$pair}[$m][$z], 6); }
			#$H = $$___CFH___[$m];
			print COM "$clean_pair ||| @output\n";
		}
	}

}

##########################################################################################################################################################

sub genConsStat {

my	($pair, $Q, $M, $A, $O, $W, $N, $GP, $GA, $GO) = @_;

my	($i, $m, $z, $ali, $ori, $weight, $l);
my	@u = ();

	#for ($i=0; $i<scalar(@{ $$M{$Q}{$pair} }); $i++) {
	#        $u[$i] = $$M{$Q}{$pair}[$i];
	#}
      
	$ali = join(' ', @{ $$A{$Q}{$pair} } );
        $ori = join(' ', @{ $$O{$Q}{$pair} } );

	$l = join(' ||| ', @{ $$M{$Q}{$pair} });

	$$GP{$pair}[0][0] = $l;
	$$GA{$pair}{$ali}[0][0] = 1;
	$$GO{$pair}{$ori}[0][0] = 1;

        #for ($m=0; $m<scalar(@u); $m++) {
        #	for ($z=0; $z<scalar(@$N); $z++) {
        #        	$weight = $u[$m] * $$W{$Q}[$z] / $$N[$z];
        #                $$GP{$pair}[$m][$z]       = $weight;
        #                $$GA{$pair}{$ali}[$m][$z] = 1;
        #                $$GO{$pair}{$ori}[$m][$z] = 1;
	#	}
	#}

}

##########################################################################################################################################################

sub collectStatistics {

my	($biString, $OchPairs, $PhrasePairHash, $Pair2Hash, $Measure, $Weight, $NormWeight, $sentLength, $sw2tw, $Align, $Orient, $word2pos, $invA, $flag, $unSegmented, $CompHash, $CompPairMeasure) = @_;

my	($pair, $hashValue, $NoCC, $compHashValue, $compPair);
my	@NoCSSG = ();
my	%local = ();
#my      $n_sigma_tau = 0;
my	$allow = 1;
my	@string = ();
my	@Unaligned; 

	@string = split(/ XXX /, $biString);
	if ( ($$unSegmented[0] eq $string[0]) && ($$unSegmented[1] eq $string[1]) ) { $allow = 0; }
	if ($flag == 0) { $allow = 1; }

	foreach $pair (keys(%$OchPairs)) {

		$hashValue = $$Pair2Hash{$pair};
		#if ( scalar(keys(%{$$PhrasePairHash{$hashValue}})) > 1 ) { next; }
		$NoCC = scalar(keys(%{$$PhrasePairHash{$hashValue}}));
		@NoCSSG = (0,0);
		@Unaligned = ();

		foreach $compHashValue (keys(%{$$PhrasePairHash{$hashValue}})) {
			
			$NoCSSG[0] += $$PhrasePairHash{$hashValue}{$compHashValue}[0];
                        $NoCSSG[1] += $$PhrasePairHash{$hashValue}{$compHashValue}[1];

			$compPair = $$CompHash{$compHashValue}[1];

			if    ($compPair =~ 'XXX NULL') { collectUnaligned(0, \@Unaligned, $word2pos, $compPair, $compHashValue);  next; }
			elsif ($compPair =~ 'NULL XXX') { collectUnaligned(1, \@Unaligned, $word2pos, $compPair, $compHashValue);  next; }
 
			storePairInfo($biString, $pair, $compPair, $compHashValue, $Measure, $CompPairMeasure, \%local, $word2pos);
			#$n_sigma_tau += $$PhrasePairHash{$hashValue}{$compHashValue};
		}

		appendUnaligned(\@Unaligned, $biString, $pair, $Measure, $CompPairMeasure, \%local, $word2pos);

		#@{ $$Measure{$biString}{$pair} } = (1, $NoCC, $NoCSSG);
		#@{ $$Measure{$biString}{$pair} } = @NoCSSG;
		#@{ $$Measure{$biString}{$pair} } = ($NoCC);
		lexAlignOrient($pair, $biString, $sw2tw, $Align, $Orient, $word2pos, $invA, $$sentLength[1]-1, $$sentLength[0]-1);
	}

	if ( $allow == 0 ) { return(); }

	$$Weight{$biString}[0] = 1;
	#$$NormWeight[0] += 1;
	$$NormWeight[0] = 1;

}

##########################################################################################################################################################

sub appendUnaligned {

my	($Unaligned, $biString, $pair, $Measure, $CompPairMeasure, $local, $word2pos) = @_;

my	($i, $u, $phrase, $compPair, $compHashValue);
my	@list;

	for ($i=0; $i<=1; $i++) {

		if (!defined($$Unaligned[$i])) { next; }
		@list = ();
		$phrase = ();
		$compPair = ();
		$compHashValue = 0;
		foreach $u ( sort{ $$Unaligned[$i]{$a}[0] <=> $$Unaligned[$i]{$b}[0]  } (keys(%{$$Unaligned[$i]})) ) { 
			push( @list, $u); 
			$compHashValue += $$Unaligned[$i]{$u}[1];
		}
		$phrase = join(' ', @list);
		if    ($i == 0) { $compPair = "$phrase XXX NULL"; }
		elsif ($i == 1) { $compPair = "NULL XXX $phrase"; }

		storePairInfo($biString, $pair, $compPair, $compHashValue, $Measure, $CompPairMeasure, $local, $word2pos);
	}

}

##########################################################################################################################################################

sub storePairInfo {

my	($biString, $pair, $compPair, $compHashValue, $Measure, $CompPairMeasure, $local, $word2pos) = @_;

my	($i, $j, $diff);
my	@_P = ();
my	@P = ();
my	@W;
my	@list;

	@P = split(/ XXX /, $compPair);
	for ($i=0; $i<=1; $i++) {
		@list = ();
		if ($P[$i] eq 'NULL') { 
			$_P[$i] = 'NULL';
			next;
		}
		@W = ();
		@W = split(/ /, $P[$i]);

		push( @list, $W[0] );
		for ($j=1; $j<scalar(@W); $j++) {

			$diff = $$word2pos[$i]{$W[$j]} - $$word2pos[$i]{$W[$j-1]};
			if ($diff == 1) { 
				push( @list, $W[$j] );
			}
			else {
				push( @list, 'GAP' );
				push( @list, $W[$j] );
                        }
		}
		$_P[$i] = join(' ', @list );
	}
	$compPair = ();
	$compPair = join(' XXX ', @_P);

	push( @{ $$Measure{$biString}{$pair} }, $compPair);
        if (defined($$local{$compHashValue})) { return(); }
        $$local{$compHashValue} = 'def';
        $compPair =~ s/XXX/\|\|\|/;
        @{ $$CompPairMeasure{$biString}{$compPair} } = (1);
}

##########################################################################################################################################################

sub collectUnaligned {

my	($I, $Unaligned, $word2pos, $compPair, $compHashValue) = @_;

my	@P = ();
my	@W = ();

	@P = split(/ XXX /, $compPair);
	@W = split(/ /, $P[$I]);
	$$Unaligned[$I]{$P[$I]}[0] = $$word2pos[$I]{$W[0]};
	$$Unaligned[$I]{$P[$I]}[1] = $compHashValue;
}

##########################################################################################################################################################

sub lexAlignOrient {

my      ($pair, $biString, $sw2tw, $Align, $Orient, $word2pos, $invA, $I_max, $J_max) = @_;

my      ($i, $j, $trg, $_j);
my      @P = ();
my      @tokens = ();
my      %trgHash = ();
my      ($I, $J, $N, $M);
my      @Order = ();

	@P = split(/ \|\|\| /, $pair);
        for ($i=0; $i<=1; $i++) { @{ $tokens[$i] } = split(/ /, $P[$i]); }
        for ($j=0; $j<scalar(@{ $tokens[1] }); $j++) { $trgHash{$tokens[1][$j]} = $j; }
        for ($j=0; $j<scalar(@{ $tokens[0] }); $j++) {
        	if (!defined($$sw2tw{$tokens[0][$j]})) { next; }
                foreach $trg (keys(%{$$sw2tw{$tokens[0][$j]}})) {
                	if (!defined($trgHash{$trg})) { next; }
                        $_j = $trgHash{$trg};
                        push( @{ $$Align{$biString}{$pair} }, "$j-$_j");
                }
       	}

        $I = $$word2pos[1]{$tokens[1][0]};
        $N = $$word2pos[1]{$tokens[1][scalar(@{$tokens[1]})-1]} - $I;
        $J = $$word2pos[0]{$tokens[0][0]};
        $M = $$word2pos[0]{$tokens[0][scalar(@{$tokens[0]})-1]} - $J;

my	@NW = ();
my	@NE = ();
my	@SW = ();
my	@SE = ();

        @NW = ($I-1, $J-1);          @NE = ($I-1, $J+$M+1);
        @SW = ($I+$N+1, $J-1);       @SE = ($I+$N+1, $J+$M+1);

        if ( ($I == 0) && ($J == 0) ) { $Order[0] = 'mono'; }
        else {
        	if    (defined($$invA{$NW[0]}{$NW[1]})) { $Order[0] = 'mono'; }
                elsif (defined($$invA{$NE[0]}{$NE[1]})) { $Order[0] = 'swap'; }
                else                                    { $Order[0] = 'other'; }
        }

        if ( ($I+$N == $I_max) && ($J+$M == $J_max) ) { $Order[1] = 'mono'; }
        else {
        	if    (defined($$invA{$SW[0]}{$SW[1]})) { $Order[1] = 'swap'; }
        	elsif (defined($$invA{$SE[0]}{$SE[1]})) { $Order[1] = 'mono'; }
        	else                                    { $Order[1] = 'other'; }
        }

        @{ $$Orient{$biString}{$pair} } = @Order;
}

##########################################################################################################################################################

sub componentsOfConsistentPairs {

my	($OchPairs, $_TrlSet_, $_SegSet_, $PhrasePairHash, $CompHash, $Pair2Hash, $sw2tw, $word2segPos, $pos2seg, $LexProbs) = @_;

my      ($pair, $hashValue, $i, $j, $pos, $seg, $random, $u, $v, $s, $t, $c, $u_l, $l, $compHashValue, $ignore); 
my	@cssg;
my      $PairGraph; 
my      @tokens;
my	@Segments;
my	@P; 
my	@ConComp = ();

my	%PairVertices;
my	@PairNoV;
my	@PairSegEdges; 
my	@PairSegEdgesSet;
my	@PairSegEdgesList;
my	%PairTrlEdgesSet; 

my	%Vertices = ();
my	@Vlocal = ();
my	@NoV = ();
my	@SegEdgesSet = ();
my	%TrlEdgesSet = ();
my	@SegEdgesList = ();

my	@word2phrasePos = (); 
my	%localWord2phrasePos = ();
my	@localList = ();

	%$Pair2Hash = ();

	foreach $pair (keys(%$OchPairs)) {

		$hashValue = 0;
		$PairGraph = ();
        	$PairGraph = Graph::Undirected->new('unionfind'=>1);
		@P = ();
		@P = split(/ \|\|\| /, $pair);
		@Segments = ();
	
	        %PairVertices = (); 
	        @PairNoV = (0,0);
		@PairSegEdges = ();
	        @PairSegEdgesSet = (); 
	        @PairSegEdgesList = ();
	        %PairTrlEdgesSet = (); 

		$ignore = 0;

	        for ($i=0; $i<=1; $i++) {

			@tokens = ();
			@tokens = split(/ /, $P[$i]);
			
			if (scalar(@tokens) > $___MaxPhraseLength___) { $ignore = 1;  last; }

			%{ $Segments[$i] } = ();
			for ($j=0; $j<scalar(@tokens); $j++) {
				$u = $tokens[$j];
				$word2phrasePos[$i]{$u} = $j;
				$pos = $$word2segPos[$i]{$u};
				$seg = $$pos2seg[$i][$pos];
				$Segments[$i]{$seg} = 'def';
				$PairVertices{"$u|$i"} = 'def';
				$PairNoV[$i]++;
				if ($i == 1) { next; }
				foreach $v (keys(%{$$sw2tw{$u}})) {
					$PairGraph->add_edge("$u|0", "$v|1");
					$hashValue += $$_TrlSet_{"$u ||| $v"};
					$PairTrlEdgesSet{"$u ||| $v"} = $$_TrlSet_{"$u ||| $v"};
				}
			}

               		foreach $seg (keys(%{ $Segments[$i] })) {
	                        @tokens = ();
        	       	        @tokens = split(/ /, $seg);
                        	if (scalar(@tokens) == 1) {
					$u = $tokens[0];
					$v = 'segVOID'; 
                                	$PairGraph->add_vertex("$u|$i");
        	                        if (!defined($$_SegSet_[$i]{"$u ||| $v"})) {
                                        	$random = rand();
	                                        $random = fewerDigits($random, 10);
        	                                $$_SegSet_[$i]{"$u ||| $v"} = $random;
                        	        }
					$hashValue += $$_SegSet_[$i]{"$u ||| $v"};
					next;
                        	}
	                        for ($j=0; $j<scalar(@tokens)-1; $j++) {
					$u = $tokens[$j];
					$v = $tokens[$j+1];
        	                        $PairGraph->add_edge("$u|$i", "$v|$i");
                        	        if (!defined($$_SegSet_[$i]{"$u ||| $v"})) {
	                                	$random = rand();
	        	                        $random = fewerDigits($random, 10);
        	        	                $$_SegSet_[$i]{"$u ||| $v"} = $random;
					}
					if ( (defined($PairSegEdgesSet[$i]{"$u ||| $v"})) || (defined($PairSegEdgesSet[$i]{"$v ||| $u"})) ) { next; }
					$hashValue += $$_SegSet_[$i]{"$u ||| $v"};
					$PairSegEdges[$i]{$u}{$v} = 'def';
					$PairSegEdges[$i]{$v}{$u} = 'def';
                                        $PairSegEdgesSet[$i]{"$u ||| $v"} = 'def';
                                        push( @PairSegEdgesList, "$u|$i" );
                                        push( @PairSegEdgesList, "$v|$i" );
                        	}
                	}
        	}

		if ($ignore == 1) { 
			delete($$OchPairs{$pair});
			next; 
		}

		$$Pair2Hash{$pair} = $hashValue;

		if ( (defined($$PhrasePairHash{$hashValue})) || (defined($$CompHash{$hashValue})) ) { 
			next; 
		}

		@ConComp = ();
		@ConComp = $PairGraph->connected_components();

		if (scalar(@ConComp) == 1) {
			#@cssg = ();
			#@cssg = weightedPowerSet(\%PairVertices, \@PairNoV, \@PairSegEdgesSet, \%PairTrlEdgesSet, \@PairSegEdgesList, $LexProbs);
			#$cssg = ConSpanSubGraphs(\%PairVertices, \@PairNoV, \@PairSegEdgesSet, \%PairTrlEdgesSet, \@PairSegEdgesList);
			@cssg = (1,1);
			@{ $$CompHash{$hashValue}[0] } = @cssg;
			$$CompHash{$hashValue}[1] = "$P[0] XXX $P[1]";

			@{ $$PhrasePairHash{$hashValue}{$hashValue} } = @cssg;
			next;
		}

		for ($c=0; $c<scalar(@ConComp); $c++) {

                        $compHashValue = 0;

			#if (scalar(@{ $ConComp[$c] }) == 1) {
			#	$u_l = $ConComp[$c][0];
			#	($u, $l) = split(/\|/, $u_l);
			#	$compHashValue = $$_SegSet_[$l]{"$u ||| segVOID"};
			#	$$CompHash{$compHashValue} = 0;
			#	$$PhrasePairHash{$compHashValue}{$compHashValue} = 0;
			#	$$PhrasePairHash{$hashValue}{$compHashValue} = 0;
			#	next; 
			#}

                        %Vertices = ();
                        @Vlocal = ();
                        foreach $u_l (@{$ConComp[$c]}) {
                        	$Vertices{$u_l} = 'def';
                                ($u, $l) = split(/\|/, $u_l);
                                if ($l == 0) { $Vlocal[0]{$u} = 'def'; }
                                else         { $Vlocal[1]{$u} = 'def'; }
                        }

                        @NoV = (0,0);
                        @NoV = ( scalar(keys(%{$Vlocal[0]})), scalar(keys(%{$Vlocal[1]})) );

			#if ( ($NoV[0] == 0) || ($NoV[1] == 0) ) { next; }

                        @SegEdgesSet = ();
                        %TrlEdgesSet = ();
                        @SegEdgesList = ();
                        for ($i=0; $i<=1; $i++) {
                                foreach $u (keys(%{$Vlocal[$i]})) {
					if (scalar(keys(%{$PairSegEdges[$i]{$u}})) != 0) {
	                                	foreach $v (keys(%{$PairSegEdges[$i]{$u}})) {
        	                                	if ( (defined($SegEdgesSet[$i]{"$u ||| $v"})) || (defined($SegEdgesSet[$i]{"$v ||| $u"})) ) { next; }
                                                        if    (defined($$_SegSet_[$i]{"$u ||| $v"})) { $compHashValue += $$_SegSet_[$i]{"$u ||| $v"}; }
							elsif (defined($$_SegSet_[$i]{"$v ||| $u"})) { $compHashValue += $$_SegSet_[$i]{"$v ||| $u"}; }
                        	                        $SegEdgesSet[$i]{"$u ||| $v"} = 'def';
                                	                push( @SegEdgesList, "$u|$i" );
                                        	        push( @SegEdgesList, "$v|$i" );
                                		}
					}
					else {	
						$compHashValue += $$_SegSet_[$i]{"$u ||| segVOID"};
					}
                                        if ($i == 1) { next; }
                                       	foreach $v (keys(%{$$sw2tw{$u}})) {
                                               	$TrlEdgesSet{"$u ||| $v"} = $$_TrlSet_{"$u ||| $v"};
                                                $compHashValue += $$_TrlSet_{"$u ||| $v"};
                                        }
                                }
			}

			if (defined($$CompHash{$compHashValue})) {
				@cssg = ();
				@cssg = @{ $$CompHash{$compHashValue}[0] };
				@{ $$PhrasePairHash{$hashValue}{$compHashValue} } = @cssg;
				next;
			}

			#@cssg = ();
			#@cssg = weightedPowerSet(\%Vertices, \@NoV, \@SegEdgesSet, \%TrlEdgesSet, \@SegEdgesList, $LexProbs);
                        #$cssg = ConSpanSubGraphs(\%Vertices, \@NoV, \@SegEdgesSet, \%TrlEdgesSet, \@SegEdgesList);
                        @cssg = (1,1);
			@{ $$PhrasePairHash{$hashValue}{$compHashValue} } = @cssg;
			@{ $$CompHash{$compHashValue}[0] } = @cssg;
			@P = ();
			for ($i=0; $i<=1; $i++) {
				%localWord2phrasePos = ();
				@localList = ();
				foreach $u (keys(%{$Vlocal[$i]})) {
					$localWord2phrasePos{$u} = $word2phrasePos[$i]{$u};
				}
				foreach $u ( sort{ $localWord2phrasePos{$a} <=> $localWord2phrasePos{$b}  } (keys(%localWord2phrasePos)) ) {
					push(@localList, $u);
				}
				$P[$i] = join(' ', @localList);
			}
			if ($P[0] eq "") {
				$$CompHash{$compHashValue}[1] = "NULL XXX $P[1]";
			}
			elsif ($P[1] eq "") {
				$$CompHash{$compHashValue}[1] = "$P[0] XXX NULL";
			}
			else {
				$$CompHash{$compHashValue}[1] = "$P[0] XXX $P[1]";
			}

			@{ $$PhrasePairHash{$compHashValue}{$compHashValue} } = @cssg;
		}
	}
}

##########################################################################################################################################################

sub weightedPowerSet {

my      ($Vertices, $NoV, $SegEdgesSet, $TrlEdgesSet, $SegEdgesList, $LexProbs) = @_;

my	@A;
my	($u_l, $u, $l, $v, $i, $full_string, $string, $n, $bit, $j);
my	@sum;
my	@prod;

	if ( ($$NoV[0] == 0) || ($$NoV[1] == 0) ) { return(0,0); }

	foreach $u_l (keys(%$Vertices)) {
		($u, $l) = split(/\|/, $u_l);
		foreach $v (keys(%{ $$LexProbs[$l]{$u} })) {
			push( @{$A[$l]}, $$LexProbs[$l]{$u}{$v});

			if ($v eq 'NULL') {
				push( @{$A[1-$l]}, $$LexProbs[1-$l]{$v}{$u});
			}
		}
	}

	@sum = (0,0);
	$n = scalar(@{$A[0]});
	for ($i=1; $i<2**($n); $i++) {
	        $full_string = dec2bin($i);
        	$string = substr($full_string, -$n, $n );
		@prod = (1,1);
	        for ($j=0; $j<$n; $j++) {
        	        $bit = substr($string, $j, 1);
                	if ($bit eq "0") { next; }
			$prod[0] *= $A[0][$j];
			$prod[1] *= $A[1][$j];
        	}
		
		$sum[0] += $prod[0];
		$sum[1] += $prod[1];
	}
				
	return($sum[0], $sum[1]);
}

##########################################################################################################################################################

sub dec2bin { return unpack("B32", pack("N", shift)); }

##########################################################################################################################################################

sub ConSpanSubGraphs {

my      ($Vertices, $NoV, $SegEdgesSet, $TrlEdgesSet, $SegEdgesList) = @_;

	#return(1);

my      ($MinNoTrl, $NoTrl, $V_0, $V_1, $E_0, $E_1, $tuple, $hashValue, $i, $u, $v, $SubGraph, $tmp);
my      @TrlList = ();
my	@LocTrlList;
my      @PowerSet = ();
my      @V = ();
my      @SegE = ();
my      %StoreSubGraphs = ();
my      @TrlE = ();
my      @ConComp = ();
my      ($NoDSSG, $NoCSSG, $NoSSG) = (0, 0, 0);
my	$MaxNoCSSG = 0;
my      ($NoSamples, $approximate) = (0, 0);
my      ($allGraphs, $_allGraphs) = (0, 0);
my      %AllGraphs = ();

        $V_0 = $$NoV[0];
        $V_1 = $$NoV[1];
        $E_0 = scalar(keys(%{$$SegEdgesSet[0]}));
        $E_1 = scalar(keys(%{$$SegEdgesSet[1]}));

        $MinNoTrl = $V_0 + $V_1 - $E_0 - $E_1 -1;

        $NoTrl = scalar(keys(%$TrlEdgesSet));

	$MaxNoCSSG = 2**$NoTrl - 1;

        if ($MinNoTrl <= 0) {
                return(0);
        }
        elsif ($MinNoTrl == 1) {
                #$NoCSSG = 2**$NoTrl - 1;
                #return($NoCSSG);
                return(1);
        }
        elsif ($MinNoTrl == $NoTrl) {
                #return(1);
                return(1/$MaxNoCSSG);
        }
        else {
                $NoSSG = 2**$NoTrl;
                for ($i=0; $i<$MinNoTrl; $i++) { $NoSSG -= N_choose_K($NoTrl, $i); }
        }

        @TrlList = keys(%$TrlEdgesSet);
        @PowerSet = subsets(\@TrlList, $MinNoTrl);

        if (scalar(@PowerSet) > $___MaxNoSubGraphSamples___) { $approximate = 1; }

        @V = keys(%$Vertices);
        @SegE = @$SegEdgesList;
        foreach $tuple (@PowerSet) {
                if ($NoSamples == $___MaxNoSubGraphSamples___) { last; }
                @TrlE = ();
                $hashValue = 0;
                @LocTrlList = ();
                for ($i=0; $i<$MinNoTrl; $i++) {
                        ($u, $v) = split(/ \|\|\| /, $$tuple[$i]);
                        push( @TrlE, "$u|0" );
                        push( @TrlE, "$v|1" );
                        $hashValue += $$TrlEdgesSet{"$u ||| $v"};
                        push( @LocTrlList, $$tuple[$i] );
                }
                $AllGraphs{$hashValue} = 'def';
                $SubGraph = ();
                $SubGraph = Graph::Undirected->new();
                $SubGraph->add_vertices(@V);
                $SubGraph->add_edges(@SegE);
                $SubGraph->add_edges(@TrlE);
                @ConComp = ();
                @ConComp = $SubGraph->connected_components();
                if (scalar(@ConComp) == 1) { $SubGraph=();  next; }

                $tmp = getComplement(\@LocTrlList, \@TrlList);
                $StoreSubGraphs{$hashValue} = [$SubGraph, scalar(@ConComp), $tmp];
                $NoSamples++;
        }
        $allGraphs = scalar(keys(%AllGraphs));

        ($NoDSSG, $_allGraphs) = iterateCSSG(\%StoreSubGraphs, $TrlEdgesSet);

        if ($approximate == 1) { $NoDSSG = floor( $NoSSG*($NoDSSG/($allGraphs+$_allGraphs)) ); }
        $NoCSSG = $NoSSG - $NoDSSG;

        #return($NoCSSG);
        return($NoCSSG/$MaxNoCSSG);
}

##########################################################################################################################################################

sub iterateCSSG {

my      ($Hash, $Weights)=@_;

my      $DiscExists = 1;
my      ($h, $G, $edge, $u, $v, $new_h, $compare, $tmp, $E, $F);
my      @ConComp;
my      @loc;
my      %NewHash;
my      $NoSSG = 0;
my      $NoDSSG = 0;
my      %AllGraphs = ();
my      $allGraphs = 0;

        $NoDSSG = scalar(keys(%$Hash));

        while($DiscExists == 1) {
                $DiscExists = 0;
                %NewHash = ();
                foreach $h ( sort{ $$Hash{$b}[1] <=> $$Hash{$a}[1] } (keys(%$Hash)) ) {
                        $G = ();
                        $G = $$Hash{$h}[0];
                        $E = ();
                        $E = $$Hash{$h}[2];
                        foreach $edge (@$E) {
                                ($u, $v) = split(/ \|\|\| /, $edge);
                                $G->add_edge("$u|0", "$v|1");
                                @ConComp = ();
                                @ConComp = $G->connected_components();
                                $AllGraphs{$h + $$Weights{$edge}} = 'def';
                                if (scalar(@ConComp) == 1) {
                                        $G->delete_edge("$u|0", "$v|1");
                                        next;
                                }
                                $DiscExists = 1;
                                $new_h = $h + $$Weights{$edge};
                                if (defined($NewHash{$new_h})) {
                                        $G->delete_edge("$u|0", "$v|1");
                                        next;
                                }
                                @loc = ();
                                @loc = ($edge);
                                $tmp = getComplement( \@loc, \@$E);

                                $F = ();
                                $F = Graph::Undirected->new();
                                $F = $G->copy_graph;
                                $NewHash{$new_h} = [$F, scalar(@ConComp), $tmp];
                                $G->delete_edge("$u|0", "$v|1");
                        }
                }
                %$Hash = ();
                %$Hash = %NewHash;
                $NoDSSG += scalar(keys(%$Hash));
        }
        $allGraphs = scalar(keys(%AllGraphs));
        return($NoDSSG,$allGraphs);
}

##########################################################################################################################################################

sub consistentPairs {

my      ($length, $segA, $trg_segA, $pos2seg, $OchPairs) = @_;

my      ($src_start, $src_end, $trg_start, $trg_end, $a, $s, $t, $unAlTrgEdg);

	%$OchPairs = ();

	if ( scalar(@{$$pos2seg[1]}) > scalar(@$trg_segA) ) { $unAlTrgEdg = scalar(@{$$pos2seg[1]}); }
	else { $unAlTrgEdg = 0; }

        for ($src_start=0; $src_start<$$length[0]; $src_start++) {
                for ($src_end=$src_start; $src_end<$$length[0]; $src_end++) {
                        if ( $src_end-$src_start+1 > $___MaxPhraseLength___ ) { last; }
                        ($trg_start, $trg_end) = ($$length[1]-1, -1);
                        foreach $a (@$segA) {
                                ($s, $t) = split(/\-/, $a);
                                if ( ($src_start <= $s) && ($s <= $src_end) ) {
                                        $trg_start = min($t, $trg_start);
                                        $trg_end   = max($t, $trg_end);
                                }
                        }
                        OchExtract($trg_start, $trg_end, $src_start, $src_end, $pos2seg, $segA, $trg_segA, $OchPairs, $unAlTrgEdg);
                }
        }
}

##########################################################################################################################################################

sub OchExtract {

my      ($f_start, $f_end, $e_start, $e_end, $segments, $A, $f_A, $OchPairs, $unAlTrgEdg) = @_;

my      ($e, $f, $f_i, $f_j, $e_phrase, $f_phrase, $a, $abort_i, $abort_j, $k);
my      @e_list = ();
my      @f_list;

        if ($f_end == -1) { return(); }
        foreach $a (@$A) {
                ($e, $f) = split(/\-/, $a);
                if ( ($f < $f_start) || ($f > $f_end) ) { next; }
                if ( ($e < $e_start) || ($e > $e_end) ) { return(); }
        }

        @e_list = @{$$segments[0]}[$e_start..$e_end];
        $e_phrase = join(' ', @e_list);
        $f_i = $f_start;
        $abort_i = 0;
	if (scalar(@e_list) > $___MaxPhraseLength___) { $abort_i = 1;  }
        while( $abort_i == 0 ) {
                $f_j = $f_end;
                $abort_j = 0;
                while( $abort_j == 0 ) {
                        @f_list = ();
                        @f_list = @{$$segments[1]}[$f_i..$f_j];
			if (scalar(@f_list) > $___MaxPhraseLength___) { $abort_j = 1; }
                        $f_phrase = ();
                        $f_phrase = join(' ', @f_list);
                        $$OchPairs{"$e_phrase ||| $f_phrase"}++;
                        $f_j++;
			if ($f_j == scalar(@$f_A)) {
				if ($unAlTrgEdg > 0) {
					for ($k=$f_j; $k<$unAlTrgEdg; $k++) {
						@f_list = ();
						@f_list = @{$$segments[1]}[$f_i..$k];
						$f_phrase = ();
						$f_phrase = join(' ', @f_list);
						$$OchPairs{"$e_phrase ||| $f_phrase"}++;
					}
				}
				$abort_j=1;
			}
			elsif (defined($$f_A[$f_j])) { $abort_j=1; }
                }
                $f_i--;
                if ( ($f_i == -1) || (defined($$f_A[$f_i])) ) { $abort_i=1; }
        }
}

##########################################################################################################################################################

sub segmentAlignment {

my      ($pos2seg, $swp2twp, $segA, $trg_segA) = @_;

my      ($position, $i, $u, $a, $j, $_position);
my      @tokens;
my      @twp2segP = ();
my      %unique = ();

        $position = 0;
        for ($i=0; $i<scalar(@{$$pos2seg[1]}); $i++) {
                @tokens = ();
                @tokens = split(/ /, $$pos2seg[1][$i]);
                for ($u=0; $u<scalar(@tokens); $u++) { $twp2segP[$position] = $i;  $position++; }
        }

	@$segA = ();
	@$trg_segA = ();
	#%$invSegA = ();

        $position = 0;
        for ($i=0; $i<scalar(@{$$pos2seg[0]}); $i++) {
                @tokens = ();
                @tokens = split(/ /, $$pos2seg[0][$i]);
                for ($u=0; $u<scalar(@tokens); $u++) {
                        if (defined($$swp2twp[$position])) {
                                for ($a=0; $a<scalar(@{$$swp2twp[$position]}); $a++) {
                                        $_position = $$swp2twp[$position][$a];
                                        $j = $twp2segP[$_position];
                                        if (!defined($unique{"$i-$j"})) {
                                                push( @$segA, "$i-$j" );
						$$trg_segA[$j] = $i;
						#$$invSegA{$j}{$i} = 'def';
                                                $unique{"$i-$j"} = 'def';
                                        }
                                }
                        }
                        $position++;
                }
        }
}

##########################################################################################################################################################

sub segmentDataStructures {

my      ($I, $x, $pos2word, $pos2seg, $seg2pos, $word2segPos, $length) = @_;

my      ($pos, $z);
my      @tokens;

        $$pos2seg[$I] = ();
        @{ $$pos2seg[$I] } = split(/ \|\|\| /, $x);

        $$seg2pos[$I] = ();
        $$word2segPos[$I] = ();

        for ($pos=0; $pos<scalar(@{ $$pos2seg[$I] }); $pos++) {
                $$seg2pos[$I]{$$pos2seg[$I][$pos]} = $pos;
                @tokens = ();
                @tokens = split(/ /, $$pos2seg[$I][$pos] );
                for ($z=0; $z<scalar(@tokens); $z++) { $$word2segPos[$I]{$tokens[$z]} = $pos; }
        }

	$$length[$I] = ();
        $$length[$I] = scalar(@{ $$pos2seg[$I] });
}

##########################################################################################################################################################

sub basicDataStructures {

my      ($sentence, $alignment, $BiSegmentation, $pos2word, $word2pos, $A, $invA, $swp2twp, $sw2tw, $_TrlSet_, $flag, $unSegmented, $sentLength) = @_;

my      ($i, $unseg, $j, $a, $s, $t, $x, $random);

        for ($i=0; $i<=1; $i++) {
                @{ $$pos2word[$i] } = split(/ /, $$sentence[$i]);
		$$unSegmented[$i] = join(' ||| ', @{ $$pos2word[$i] } );

                for ($j=0; $j<scalar(@{$$pos2word[$i]}); $j++) {
                        $$word2pos[$i]{$$pos2word[$i][$j]} = $j;
		}

        	$$sentLength[$i] = scalar(@{ $$pos2word[$i] });
	}

	if (!defined($$BiSegmentation{"$$unSegmented[0] XXX $$unSegmented[1]"})) {
		@{ $$BiSegmentation{"$$unSegmented[0] XXX $$unSegmented[1]"} } = ('VOID');
		$flag = 1;
	}

        @$A = split(/ /, $alignment);
        foreach $a (@$A) {
                ($s, $t) = split(/\-/, $a);
		$$invA{$t}{$s} = 'def';
                push( @{ $$swp2twp[$s] }, $t );
                $$sw2tw{ $$pos2word[0][$s] }{ $$pos2word[1][$t] } = 'def';
                $random = rand();
                $random = fewerDigits($random, 10);
                $$_TrlSet_{"$$pos2word[0][$s] ||| $$pos2word[1][$t]"} = $random;
        }

	return($flag);
}

##########################################################################################################################################################

sub storeInput {

my	($line, $DataStruct, $NBL) = @_;

my	($s, $t);
my	($info, $p_t2s, $p_s2t);

	if ($NBL != -1) {
		($s, $t, $info) = split(/ XXX /, $line);

        	$s =~ s/<s> //;
	        $s =~ s/ <\/s>//;

        	$t =~ s/<s> //;
	        $t =~ s/ <\/s>//;

		if (scalar(keys(%$DataStruct)) == $NBL) { return(); }
	
		@{ $$DataStruct{"$s XXX $t"} } = ($info);
	}
	else {
		($s, $t, $info) = split(/ \|\|\| /, $line);
		($p_t2s, $p_s2t) = split(/ /, $info);
		$$DataStruct[0]{$s}{$t} = $p_s2t; #print "$s --> $t ||| $p_s2t\n";
		$$DataStruct[1]{$t}{$s} = $p_t2s; #print "$t --> $s ||| $p_t2s\n";
	}
}

##########################################################################################################################################################

sub read_line {

my      ($FH) = @_;
my      $line;

        if ($FH and $line = <$FH>) {
                chomp($line);
                return($line);
        }
        return;
}

##########################################################################################################################################################

sub fewerDigits {

my      ($x, $NoD)=@_;
my      ($integer, $fractional, $less, $mantissa, $exponent);

        if ($x !~ 'e') {
                ($integer, $fractional) = split(/\./, $x);
                if (defined($fractional)) {
                        if (length($fractional) > $NoD) {
                                $less = substr($fractional, 0, $NoD);
                                $x = "$integer.$less";
                        }
                }
        }
        else {
                ($mantissa, $exponent) = split(/e-/, $x);
                ($integer, $fractional) = split(/\./, $mantissa);
                if (defined($fractional)) {
                        if (length($fractional) > $NoD) {
                                $less = substr($fractional, 0, $NoD);
                                $x = "$integer.$less".'e-'.$exponent;
                        }
                }
        }
        return($x);
}

##########################################################################################################################################################

sub N_choose_K {

my      ($n, $k)=@_;

my      $r=1;
        $r *= $_/($n-$_+1)for(1+$k..$n);
        return($r);
}

##########################################################################################################################################################

sub getComplement {

my      ($Some, $Full)=@_;
my      $compare = ();
        $compare = List::Compare->new('-a', \@$Some, \@$Full);
my      @_Some = ();
        @_Some = $compare->get_complement;
my      $ref = ();
        $ref = [@_Some];
        return($ref);
}

##########################################################################################################################################################

sub openOutputFiles {

my	($T) = @_;

my	@X = ('phrase', 'align', 'orient');
#my	@M = ('cc', 'cssg', 'occurr');
my      @M = ('cnt');
#my	@C = ('cnt');

my	($i, $j, $name, $p);
my	@F = ();
my	@P = ();

	if ($T eq 'segms') {
		#@P = ('segms', 'dsegms');
		@P = ('segms');
		for ($p=0; $p<scalar(@P); $p++) {
			for ($i=1; $i<scalar(@X); $i++) {
				for ($j=0; $j<scalar(@M); $j++) {
					$name = "$P[$p].$X[$i].$M[$j].$batch.$proc";
					open($F[$p][$i][$j], ">$name");
				}
			}
		}
		open(IDP, ">id.phrase.$batch.$proc");
		open(UNI, ">unions.phrase.$batch.$proc");
		open(COM, ">comps.phrase.$batch.$proc");	
	}
	elsif ($T eq 'words') {
		for ($i=0; $i<scalar(@X); $i++) {
			$name = "$T.$X[$i].$batch.$proc";
			open($F[$i], ">$name");
		}
	}
		
	return(\@F);
}		

##########################################################################################################################################################

sub closeOutputFiles {

my	($T, $F) = @_;
my	($i, $j, $p);

	if ($T eq 'segms') {
		for ($p=0; $p<scalar(@$F); $p++) {
			for ($i=0; $i<scalar(@{ $$F[$p] }); $i++) {
				if (!defined($$F[$p][$i])) { next; }
				for ($j=0; $j<scalar(@{ $$F[$p][$i] }); $j++) {
					close($$F[$p][$i][$j]);
				}
			}
		}
	}
	elsif ($T eq 'words') {
		for ($i=0; $i<scalar(@$F); $i++) {
			close($$F[$i]);
		}
	}

	close(IDP);
	close(IDP);
	close(COM);
}


