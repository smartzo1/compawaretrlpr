#!/usr/bin/perl -w

my ($proc, $batch, $Cont, $Disc) = @ARGV;

if(@ARGV!=4) {
    print STDERR "\nUSAGE ::: <proc #>  <batch #>  <unions.phrase.batch.proc>  <gappy.phrase.batch.proc>\n";
    exit(-1);
}
################################################################################################

system("cat  $Cont  $Disc  >> allUnions.phrase.$batch.$proc");

system("touch _WORK_.$proc");
