#!/usr/bin/perl -w

($Cont, $Disc, $batch, $packets, $nops) = @ARGV;

if(@ARGV!=5) {
    print STDERR "\nUSAGE ::: <unions.phrase.batch (DIR)>   <gappy.phrase.batch (DIR)>   <batch #>   <# packets>  <# procs>\n";
    exit(-1);
}

use POSIX;
###################################################################################################################

$packet_size = floor( $nops / $packets );
@handler=();
for ($k=0; $k<$packets; $k++ ) {
        $handler[$k][0] = $k*$packet_size+1;
        $handler[$k][1] = ($k+1)*$packet_size;
}
$handler[0][0]=0;
$handler[$packets-1][1] = $nops;

for ($k=0; $k<$packets; $k++) {
        for ($job=$handler[$k][0]; $job<=$handler[$k][1]; $job++) {
		$C = "$Cont/*.$job";
		$D = "$Disc/*.$job";
		system("nohup ./compute-cat_unions_with_gappy.pl  $job  $batch  $C  $D  &");
        }
        flagging($handler[$k][0], $handler[$k][1],  "_WORK_");
}

system("mkdir  allUnions.phrase.$batch");
system("mv     allUnions.phrase.$batch.*   allUnions.phrase.$batch");

system("touch  DONE.ALLUNI");

###################################################################################################################

sub flagging {

my      ($start, $finish, $name)=@_;
my      $job;
my      @flag_job=();
my      $flags=0;
my      $NoJ;

        $NoJ = $finish - $start + 1;

        for ($job=$start; $job <= $finish; $job++) { $flag_job[$job]=0; }

        while($flags != $NoJ) {
                for ($job=$start; $job <= $finish; $job++) {
                        if (-e "$name.$job") { $flag_job[$job]=1;}
                }
                $flags=0;
                for ($job=$start; $job <= $finish; $job++) {
                        $flags += $flag_job[$job];
                }
                sleep(1);
        }
        for ($job=$start; $job <= $finish; $job++) { qx(rm $name.$job);  }
        return();
}
